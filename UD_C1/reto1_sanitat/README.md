# Reto 1: Consultas básicas

Cristian G Guerrero.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/la-senia-db-2024/db/

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
HOSPITAL_COD AS "Código",
NOM AS "Nombre",
TELEFON AS "Teléfono"
FROM HOSPITAL;
```

## Query 2
Para hacer esta consulta, he usado la base de la query anterior, pero añadiendo el `WHERE` para insertar un `like` (es un operador para trabajar con cadena de carácteres) El `_` se utiliza para indicar que tiene más carácteres delante o detrás (en este caso solo tendría 1 más) y el `%` para indicarle que diga el resto de carácteres que siguen.

```sql
SELECT
HOSPITAL_COD AS "Código",
NOM AS "Nombre",
TELEFON AS "Teléfono"
FROM HOSPITAL
WHERE NOM LIKE "_a%";
```

## Query 3
Para seleccionar el código hospital, código sala, número empleado y apellido de los trabajadores existentes utilizo los 4 siguientes atributos: `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO` y `COGNOM` de la tabla `PLANTILLA`.

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA;
```

## Query 4
Para seleccionar el `código hospital`, `código sala`, `número empleado` y `apellido` de los trabajadores que no sea de noche, lo que hago es seleccionar la columna de `TORN` de la tabla de `PLANTILLA`. Si es distino que `N`, saldrá.

```sql
SELECT TORN
FROM PLANTILLA
WHERE TORN != "N";
```

## Query 5
Para mostrar los enfermos nacidos en 1960 lo que hago es seleccionar todo de la tabla de `MALALT` uso el `YEAR` y le digo que sea de 1960.

```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX) = 1960;
```

## Query 6
Para mostrar los nacidos a partir del 1960, uso la misma base que en la query anterior, pero le añado un `>=` para indicarle que mientras sea mayor o igual que 1960 aparezca.

```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX) >= "1960";
```
