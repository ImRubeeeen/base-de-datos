-- Uso 3 JOINS para relacionar la tabla de "usuarios" con "comentarios", "comentarios" con "comentariosFotos" y "comentariosFotos"
-- con "fotos" y con el WHERE le indico las condiciones.

SELECT usuarios.nombre, comentarios.comentario, fotos.url
FROM usuarios
JOIN comentarios 
ON usuarios.idUsuario = comentarios.idUsuario
JOIN comentariosFotos 
ON comentarios.idComentario = comentariosFotos.idComentario
JOIN fotos 
ON comentariosFotos.idFoto = fotos.idFoto
WHERE fotos.idUsuario = 11 AND fotos.idFoto = 12 AND usuarios.idUsuario = 36;

-- Forma de hacerlo sin la última parte de la consulta (se necesitan subconsultas y de momento nada, 
-- Esta parte hecha en la corrección de clase)

SELECT *
FROM fotos
JOIN comentariosFotos
ON fotos.idFoto = comentariosFotos.idFoto
JOIN comentarios
ON comentarios.idComentario = comentariosFotos.idComentario
WHERE comentarios.idUsuario = 36
	AND fotos.idFoto = 11;