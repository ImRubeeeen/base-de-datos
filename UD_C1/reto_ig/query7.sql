/* Número de fotos tomadas en la playa (en base al título) */

SELECT COUNT(*) AS 'Nº Fotos'
FROM fotos
WHERE descripcion
	LIKE '%playa%';