SELECT COUNT(RC.idTipoReaccion) AS Num_MeGusta, U.*
FROM usuarios AS U
JOIN roles AS R 
ON U.idRol = R.idRol
JOIN reaccionesComentarios AS RC 
ON U.idUsuario = RC.idUsuario
JOIN comentarios AS C ON RC.idComentario = C.idComentario
WHERE R.idRol = 3 AND RC.idTipoReaccion = 1
GROUP BY U.idUsuario
HAVING Num_MeGusta > 2;

-- OTRA FORMA (corrección de clase)

SELECT *
FROM roles
JOIN usuarios
ON usuarios.idRol = roles.idRol
LEFT JOIN reaccionesFotos
ON usuarios.idUsuario = reaccionesFotos.idUsuario
LEFT JOIN reaccionesComentarios
ON usuarios.idUsuario = reaccionesComentarios.idUsuario
WHERE roles