/* Fotos del usuario con ID 36 tomadas en enero del 2023 */

SELECT fotos.idFoto, fotos.idUsuario, fotos.fechaCreacion, usuarios.nombre
FROM fotos, usuarios
WHERE YEAR(fechaCreacion)=2023
	AND MONTH(fechaCreacion)=1
    AND usuarios.idUsuario=36
    AND fotos.idUsuario = usuarios.idUsuario;