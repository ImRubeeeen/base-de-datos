/* Fotos que han sorprendido al usuario 25 (en mi caso he usado el 11, ya que no estaba creado */

SELECT fotos.idFoto, fotos.descripcion, tiposReaccion.descripcion
FROM reaccionesFotos, fotos, tiposReaccion
WHERE reaccionesFotos.idUsuario = 11
	AND reaccionesFotos.idFoto = fotos.idFoto
    AND reaccionesFotos.idTipoReaccion = 4
    AND tiposReaccion.idTipoReaccion = reaccionesFotos.idTipoReaccion;