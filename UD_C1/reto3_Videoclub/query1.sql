-- Con JOIN:

SELECT PELICULA.Titol, GENERE.Descripcio
FROM PELICULA
JOIN GENERE 
ON PELICULA.CodiGenere = GENERE.CodiGenere;
 
-- El JOIN se encarga de unir la tabla del FROM con la de él
-- El ON se encarga se de que si hay 2 columnas iguales en diferentes tablas y las relaciono no se repitan

-- Sin JOIN:

SELECT PELICULA.Titol, GENERE.Descripcio
FROM PELICULA, GENERE
WHERE PELICULA.CodiGenere = GENERE.CodiGenere;