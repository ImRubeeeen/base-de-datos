# Reto 1: Consultas básicas

Rubén Martí Garcia.

En este reto trabajamos con la base de datos `empresa` y `videoclub` que nos viene dada en los ficheros `empresa.sql` y `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

## Query 1
Para seleccionar el `código` y `descripción` de todos los productos que comercializa la empresa, seleccionaremos estos 2 atributos, que se corresponden con las columnas: `PROD_NUM` y `DESCRIPCIO`, respectivamente, de la tabla `PRODUCTE`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
	PROD_NUM AS "Nº Producto",
    DESCRIPCIO AS "Descripción"
FROM PRODUCTE;
```

## Query 2
Para seleccionar el `código` y `descripción` de todos los productos que contienen a palabra `tenis`, seleccionaremos estos 2 atributos, que se corresponden con las columnas: `PROD_NUM` y `DESCRIPCIO`, respectivamente, de la tabla `PRODUCTE`. También usaremos el `like` junto al `%` y lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
	PROD_NUM AS "Nº Producto",
    DESCRIPCIO AS "Descripción"
FROM PRODUCTE
WHERE DESCRIPCIO like "%tennis%";
```

## Query 3
Para seleccionar el `nombre`, `área` y `teléfono` de los de los clientes existentes, usando las columnas `NOM`, `AREA` y `TELEFON` de la tabla `CLIENT`. Usamos la siguiente sentencia de sql:

```sql
SELECT
	CODI_POSTAL AS "Código",
	NOM AS "Nombre",
	AREA AS "Area",
    TELEFON AS "Teléfono"
FROM CLIENT;
```

## Query 4
Para mostrar los clientes (`código`, `nombre` y `ciudad`) que no son del área telefónica lo que hago es seleccionar la columna de `NOM`, `AREA` y `TELEFON` de la tabla de `CLIENT`. Usamos la siguiente sentencia sql:

```sql
SELECT
	CODI_POSTAL AS "Código",
	NOM AS "Nombre",
	AREA AS "Area",
    TELEFON AS "Teléfono"
FROM CLIENT
WHERE AREA != 636;
```

## Query 5
Para mostrar las ordenes de compra de la tabla de pedidos(`código`, `fecha entrega` y `fecha envío`) lo que hago es seleccionar la columna de `CLIENT_COD`, `DATA_TRAMESA` y `COM_DATA` de la tabla de `COMANDA`. Usamos la siguiente sentencia sql:

```sql
SELECT
	CLIENT_COD AS "Código",
    DATA_TRAMESA AS "Fecha entrega",
    COM_DATA AS "Fecha envío"
FROM COMANDA;
```

## Query 6
Para mostrar la lista de `nombres` y `teléfonos` de los clientes lo que hago es seleccionar la columna de `Nom`, `Telefon` de la tabla de `CLIENT`. Usamos la siguiente sentencia sql:

```sql
SELECT 
	CLIENT.Nom AS "Nombre",
    CLIENT.Telefon AS "Teléfono"
FROM CLIENT;
```

## Query 7
Para mostrar la lista de `fechas` e `importes` de las facturas lo que hago es seleccionar la columna de `Data` e `Importe` de la tabla de `FACTURA`. Usamos la siguiente sentencia sql:

```sql
SELECT 
	FACTURA.Data AS "Fecha",
    FACTURA.Import AS "Importe"
FROM FACTURA;
```