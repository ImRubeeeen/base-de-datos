SELECT
	CODI_POSTAL AS "Código",
	NOM AS "Nombre",
	AREA AS "Area",
    TELEFON AS "Teléfono"
FROM CLIENT
WHERE AREA != 636;
	