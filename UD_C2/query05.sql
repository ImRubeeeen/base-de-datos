SELECT Al.AlbumId, Al.Title
FROM Artist A
JOIN Album Al
ON A.ArtistId = Al.ArtistId
WHERE A.ArtistId IN (
	SELECT AlbumId
    FROM Album
);