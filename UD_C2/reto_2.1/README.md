# Reto 1: Consultas con funciones de agregación

Rubén Martí Garcia.

En este reto trabajamos con la base de datos `chinook`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

## Query 1
Encuentra todos los clientes de Francia.

```sql
-- Lo que hago es seleccionar la tabla de "Customer" y decirle que me saque 
-- Todos los datos de las personas que son de Francia con el "WHERE".

SELECT * FROM Chinook.Customer
WHERE Customer.Country = "France";
```

## Query 2
Muestra las facturas del primer trimestre de este año.

```sql
-- Selecciono todo de la tabla de Invoice y con el "BETWEEN" le indico entre que fechas quiero que salgan los datos.

SELECT * FROM Chinook.Invoice
WHERE Invoice.InvoiceDate BETWEEN "2024-01-01" AND "2024-03-31";

-- OTRA FORMA:

SELECT *
FROM Invoice
WHERE QUARTER(InvoiceDate) = 1
AND YEAR(InvoiceDate) = YEAR(CURRENT_DATE());
```

## Query 3
Muestra todas las canciones compuestas por AC/DC.

```sql
-- Selecciono de la tabla Track todos los nombres de canciones que ha hecho AC/DC.

SELECT Track.Name 
FROM Chinook.Track
WHERE Track.Composer = "AC/DC";
```

## Query 4
Muestra las 10 canciones que más tamaño ocupan.

```sql
-- Selecciono las 10 primeras canciones de formas descendente gracias al "ORDER BY DESC" que se encarga de ordenar de forma
-- descendente y para que me salgan solo 10 le pongo el "LIMIT".

SELECT T.Name, T.Bytes
FROM Chinook.Track AS T
ORDER BY Bytes DESC
LIMIT 10;
```

## Query 5
Muestra el nombre de aquellos países en los que tenemos clientes.

```sql
-- Selecciono todos los países de donde son los clientes (sin repetir un solo páis, gracias al 'DISTINCT')

SELECT DISTINCT C.Country AS Páises
FROM Chinook.Customer AS C;
```

## Query 6
Muestra todos los géneros musicales.

```sql
-- Selecciono todos los generos músicales

SELECT G.Name
FROM Chinook.Genre AS G;
```

## Query 7
Muestra todos los artistas junto a sus álbumes.

```sql
-- 

SELECT Ar.Name, Al.Title
FROM Artist AS Ar
JOIN Album AS Al 
    ON Ar.ArtistId = Al.ArtistId;

```

## Query 8
Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.

```sql
SELECT 
	E1.EmployeeId AS "ID Empleado", 
	CONCAT(E1.LastName, " ", E1.FirstName) AS "Empleado", 
    E2.EmployeeId AS "ID Supervisor", 
	CONCAT(E2.LastName, " ", E2.FirstName) AS "Supervisor"
FROM Employee AS E1
JOIN Employee AS E2
	ON E1.ReportsTo = E2.EmployeeId
ORDER BY E1.BirthDate DESC
LIMIT 5;
```

## Query 9
Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación, código postal, país, importe (en este orden).

```sql
-- Lo primero de todo concateno el nombre con el apellido (con un espacio), ya que quiero el nombre completo, uso el JOIN para relacionar las tablas de "Invoice" con "Customer" y con el WHERE le indico
-- la ciudad que quiero, en este caso solo los que son de Berlín.

SELECT 
    I.InvoiceDate, 
    CONCAT(C.FirstName, ' ', C.LastName) AS FullName, 
    I.BillingAddress, 
    I.BillingPostalCode, 
    I.BillingCity, 
    I.Total
FROM Chinook.Invoice AS I
JOIN Chinook.Customer AS C
	ON I.CustomerId = C.CustomerId
WHERE I.BillingCity = "Berlin";

/* AQUÍ OTRA FORMA QUE HE PROBADO PARA VER SI FUNCIONA DE LA MISMA FORMA (DA EL MISMO RESULTADO):

SELECT 
	I.InvoiceDate, 
	CONCAT(C.FirstName, ' ', C.LastName) AS FullName, 
	I.BillingAddress, 
    I.BillingPostalCode,
    I.BillingCountry,
    I.Total
FROM Chinook.Invoice AS I
JOIN Chinook.Customer AS C
	ON I.CustomerId = C.CustomerId
		AND I.BillingCity = C.City
		AND I.BillingCity = "Berlín";
*/
```

## Query 10
Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.

```sql
SELECT *
FROM Playlist AS P 
JOIN PlaylistTrack AS PT
ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T
ON PT.TrackId = T.TrackId
WHERE P.Name LIKE "C%"
ORDER BY T.AlbumId ASC, T.Milliseconds DESC;

-- OTRA FORMA:

SELECT *
FROM Playlist AS P 
JOIN PlaylistTrack AS PT
ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T
ON PT.TrackId = T.TrackId
JOIN Album AS A
ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE "C%"
ORDER BY A.Title ASC, T.Milliseconds DESC;
```

## Query 11
Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados
por apellido.

```sql
-- Selecciono la tabla de la izquierda "Custumer" con un FROM y la de la derecha el "Invoice" con un JOIN, si son iguales las IDs, que me pille las facturas con un total mayor a 10. 
-- todo esto lo ordeno por apellido.

SELECT 
	C.FirstName, 
	C.LastName, 
    I.Total 
FROM Customer C
JOIN Invoice I ON C.CustomerId = I.CustomerId 
WHERE I.Total > 10 
ORDER BY C.LastName;
```

## Query 12
Muestra el importe medio, mínimo y máximo de cada factura.

```sql
-- Selecciono el precio de cada factura (en la columna se representa como "Total") y con el AVG(Nombre_Colummna) saca la media de la tabla seleccionada, 
-- el MIN(Nombre_Colummna) saca el mínimo de la tabla seleccionada y el MAX(Nombre_Colummna) saca el máximo de la tabla seleccionada (la columna de "Total "sale de la tabla de "Invoice").
-- Gracias al 'GROUP BY' del InvoiceId lo que hago es que saca la media, el mínimo y el máximo de cada una, ya que si no estuviera el resultado sería la media, min y max de todas las facturas
-- y lo que queremos es sacar la factura de forma individual.

SELECT 
	AVG(Total) AS ImportePromedio, 
    MIN(Total) AS ImporteMinimo, 
    MAX(Total) AS ImporteMaximo
FROM Invoice
GROUP BY InvoiceId;
```

## Query 13
Muestra el número total de artistas.

```sql
-- Lo que hago es contar todo de la tabla de de Artistas para que me de el total de artistas.

SELECT COUNT(*) AS Artistas_Totales
FROM chinook.Artist;
```

## Query 14
Muestra el número de canciones del álbum “Out Of Time”.

```sql
-- Cuento todas las canciones que hay en álbum de "Out Of Time" gracias al 'COUNT(*)' y haciendo una relación de 2 tablas (la de "Track" y la de "Album")

SELECT COUNT(*) AS Numero_Canciones
FROM Track AS T
JOIN Album AS A 
	ON T.AlbumId = A.AlbumId
WHERE A.Title = 'Out Of Time';
```

## Query 15
Muestra el número de países donde tenemos clientes.

```sql
-- Cuento todos los páises que hay de la tabla de "customer" y con el 'DISTINCT' lo que hago es que si hay un país repetido, que no me lo cuente.

SELECT COUNT(DISTINCT customer.Country) AS Paises_Clientes
FROM chinook.customer;
```

## Query 16
Muestra el número de canciones de cada género (deberá mostrarse el nombre del
género).

```sql
-- Selecciono el nombre del Genero y cuento las IDs de las canciones, agrupandolos por el nombre de género

SELECT G.Name, 
	COUNT(T.TrackId) AS Numero_Canciones
FROM Track AS T
JOIN Genre AS G
	ON T.GenreId = G.GenreId
GROUP BY G.Name;

-- OTRA FORMA: 

SELECT G.Name, 
	COUNT(*) AS Numero_Canciones
FROM Track AS T
JOIN Genre AS G
	ON T.GenreId = G.GenreId
GROUP BY G.Name
ORDER BY COUNT(T.TrackId) DESC;
```

## Query 17
Muestra los álbumes ordenados por el número de canciones que tiene cada uno.

```sql
-- Selecciono los titulos de los albumes y cuento el número de IDs de las canciones. 
-- Agrupo por el título de los albumes y los ordeno de forma descendente el número de canciones.

SELECT A.AlbumId, A.Title, COUNT(T.TrackId) AS Numero_Canciones
FROM Album AS A
JOIN Track AS T
	ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
ORDER BY Numero_Canciones DESC;
```

## Query 18
Encuentra los géneros musicales más populares (los más comprados).

```sql
-- Canciones que más se han vendido
SELECT TrackId, COUNT(*)
FROM InvoiceLine
GROUP BY TrackId
ORDER BY COUNT(*) DESC;

-- Géneros que más se han vendido
SELECT Genre.GenreId, Genre.Name, COUNT(*)
FROM InvoiceLine
JOIN Track
ON Track.TrackId = InvoiceLine.TrackId
JOIN Genre
ON Genre.GenreId = Track.GenreId
GROUP BY Genre.GenreId
ORDER BY COUNT(*) DESC;
```

## Query 19
Lista los 6 álbumes que acumulan más compras.

```sql
-- Álbumes que más se han vendido
SELECT Album.AlbumId , Album.Title, COUNT(*) AS Total
FROM InvoiceLine
JOIN Track
ON Track.TrackId = InvoiceLine.TrackId
JOIN Album
ON Album.AlbumId = Track.AlbumId
GROUP BY Album.AlbumId
ORDER BY COUNT(*) DESC
LIMIT 6;
```

## Query 20
Muestra los países en los que tenemos al menos 5 clientes.

```sql
-- Selecciono los países de la tabla "customer" y los agrupo por países. Lo que tiene esta consulta es que está el 'HAVING COUNT' que lo hace es contar y filtrar de una columna, en este caso
-- cuenta y filtra los que sean mayor o igual que 5.

SELECT Country 
FROM customer
GROUP BY Country 
HAVING COUNT(CustomerId) >= 5;
```