-- Cuento todos los páises que hay de la tabla de "customer" y con el 'DISTINCT' lo que hago es que si hay un país repetido, que no me lo cuente.

SELECT COUNT(DISTINCT customer.Country) AS Paises_Clientes
FROM chinook.customer;