-- Selecciono los países de la tabla "customer" y los agrupo por países. Lo que tiene esta consulta es que está el 'HAVING COUNT' que lo hace es contar y filtrar de una columna, en este caso
-- cuenta y filtra los que sean mayor o igual que 5.

SELECT Country 
FROM customer
GROUP BY Country 
HAVING COUNT(CustomerId) >= 5;