-- Selecciono el precio de cada factura (en la columna se representa como "Total") y con el AVG(Nombre_Colummna) saca la media de la tabla seleccionada, 
-- el MIN(Nombre_Colummna) saca el mínimo de la tabla seleccionada y el MAX(Nombre_Colummna) saca el máximo de la tabla seleccionada (la columna de "Total "sale de la tabla de "Invoice").
-- Gracias al 'GROUP BY' del InvoiceId lo que hago es que saca la media, el mínimo y el máximo de cada una, ya que si no estuviera el resultado sería la media, min y max de todas las facturas
-- y lo que queremos es sacar la factura de forma individual.

SELECT 
	AVG(Total) AS ImportePromedio, 
    MIN(Total) AS ImporteMinimo, 
    MAX(Total) AS ImporteMaximo
FROM Invoice
GROUP BY InvoiceId;