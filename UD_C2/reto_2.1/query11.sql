-- Selecciono la tabla de la izquierda "Custumer" con un FROM y la de la derecha el "Invoice" con un JOIN, si son iguales las IDs, que me pille las facturas con un total mayor a 10. 
-- todo esto lo ordeno por apellido.

SELECT 
	C.FirstName, 
	C.LastName, 
    I.Total 
FROM Customer C
JOIN Invoice I ON C.CustomerId = I.CustomerId 
WHERE I.Total > 10 
ORDER BY C.LastName;