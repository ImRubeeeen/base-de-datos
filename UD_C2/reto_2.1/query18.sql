-- Canciones que más se han vendido
SELECT TrackId, COUNT(*)
FROM InvoiceLine
GROUP BY TrackId
ORDER BY COUNT(*) DESC;

-- Géneros que más se han vendido
SELECT Genre.GenreId, Genre.Name, COUNT(*)
FROM InvoiceLine
JOIN Track
ON Track.TrackId = InvoiceLine.TrackId
JOIN Genre
ON Genre.GenreId = Track.GenreId
GROUP BY Genre.GenreId
ORDER BY COUNT(*) DESC;