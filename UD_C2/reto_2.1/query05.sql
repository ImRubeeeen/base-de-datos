-- Selecciono todos los países de donde son los clientes (sin repetir un solo páis, gracias al 'DISTINCT')

SELECT DISTINCT C.Country AS Páises
FROM Chinook.Customer AS C;