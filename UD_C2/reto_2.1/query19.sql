-- Álbumes que más se han vendido
SELECT Album.AlbumId, Album.Title, Artist.Name, COUNT(*) AS Total
FROM InvoiceLine
JOIN Track
ON Track.TrackId = InvoiceLine.TrackId
JOIN Album
ON Album.AlbumId = Track.AlbumId
JOIN Artist
ON Album.ArtistId = Artist.ArtistId
GROUP BY Album.AlbumId
ORDER BY COUNT(*) DESC
LIMIT 6;