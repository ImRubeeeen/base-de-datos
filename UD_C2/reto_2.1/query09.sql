-- Lo primero de todo concateno el nombre con el apellido, ya que quiero el nombre completo, uso el JOIN para relacionar las tablas de "Invoice" con "Customer" y con el WHERE le indico
-- la ciudad que quiero, en este caso solo los que son de Berlín.

SELECT 
    I.InvoiceDate, 
    CONCAT(C.FirstName, ' ', C.LastName) AS FullName, 
    I.BillingAddress, 
    I.BillingPostalCode, 
    I.BillingCity, 
    I.Total
FROM Chinook.Invoice AS I
JOIN Chinook.Customer AS C
	ON I.CustomerId = C.CustomerId
WHERE I.BillingCity = "Berlin";

/* AQUÍ OTRA FORMA QUE HE PROBADO PARA VER SI FUNCIONA DE LA MISMA FORMA (DA EL MISMO RESULTADO): 
SELECT 
	I.InvoiceDate, 
	CONCAT(C.FirstName, ' ', C.LastName) AS FullName, 
	I.BillingAddress, 
    I.BillingPostalCode,
    I.BillingCountry,
    I.Total
FROM Chinook.Invoice AS I
JOIN Chinook.Customer AS C
	ON I.CustomerId = C.CustomerId
		AND I.BillingCity = C.City
		AND I.BillingCity = "Berlín";
*/