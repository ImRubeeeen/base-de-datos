-- Selecciono el nombre del Genero y cuento las IDs de las canciones, agrupandolos por el nombre de género

SELECT G.Name, 
	COUNT(T.TrackId) AS Numero_Canciones
FROM Track AS T
JOIN Genre AS G
	ON T.GenreId = G.GenreId
GROUP BY G.Name;

-- OTRA FORMA: 

SELECT G.Name, 
	COUNT(*) AS Numero_Canciones
FROM Track AS T
JOIN Genre AS G
	ON T.GenreId = G.GenreId
GROUP BY G.Name
ORDER BY COUNT(T.TrackId) DESC;