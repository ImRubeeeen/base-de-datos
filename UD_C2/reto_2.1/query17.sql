-- Selecciono los titulos de los albumes y cuento el número de IDs de las canciones. 
-- Agrupo por el título de los albumes y los ordeno de forma descendente el número de canciones.

SELECT A.AlbumId, A.Title, COUNT(T.TrackId) AS Numero_Canciones
FROM Album AS A
JOIN Track AS T
	ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
ORDER BY Numero_Canciones DESC;