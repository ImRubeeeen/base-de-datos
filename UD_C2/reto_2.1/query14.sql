-- Cuento todas las canciones que hay en álbum de "Out Of Time" gracias al 'COUNT(*)' y haciendo una relación de 2 tablas (la de "Track" y la de "Album")

SELECT COUNT(*) AS Numero_Canciones
FROM Track AS T
JOIN Album AS A 
	ON T.AlbumId = A.AlbumId
WHERE A.Title = 'Out Of Time';