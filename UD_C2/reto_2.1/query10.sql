SELECT *
FROM Playlist AS P 
JOIN PlaylistTrack AS PT
ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T
ON PT.TrackId = T.TrackId
WHERE P.Name LIKE "C%"
ORDER BY T.AlbumId ASC, T.Milliseconds DESC;

-- OTRA FORMA:

SELECT *
FROM Playlist AS P 
JOIN PlaylistTrack AS PT
ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T
ON PT.TrackId = T.TrackId
JOIN Album AS A
ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE "C%"
ORDER BY A.Title ASC, T.Milliseconds DESC;