

SELECT Ar.Name AS Ar, Al.Title AS Album
FROM Artist AS Ar
JOIN Album AS Al
ON Ar.ArtistId = Al.ArtistId;