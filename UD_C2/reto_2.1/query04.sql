-- Selecciono las 10 primeras canciones de formas descendente gracias al "ORDER BY DESC" que se encarga de ordenar de forma
-- descendente y para que me salgan solo 10 le pongo el "LIMIT".

SELECT Track.Name, Track.Bytes
FROM Chinook.Track
ORDER BY Bytes DESC
LIMIT 10;