SELECT 
	E1.EmployeeId AS "ID Empleado", 
	CONCAT(E1.LastName, " ", E1.FirstName) AS "Empleado", 
    E2.EmployeeId AS "ID Supervisor", 
	CONCAT(E2.LastName, " ", E2.FirstName) AS "Supervisor"
FROM Employee AS E1
JOIN Employee AS E2
	ON E1.ReportsTo = E2.EmployeeId
ORDER BY E1.BirthDate DESC
LIMIT 5;