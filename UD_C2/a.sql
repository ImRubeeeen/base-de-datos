-- En esta consulta lo que he hecho ha sido unir Employee consigo mismo diferenciandolo con diferentes alias. Al hacer el 'LEFT JOIN' devuelve todos los registros del lado izquierdo aunque
-- no haya coincidencia con la tabla de la derecha. El ReportsTo relaciona la jerarquia que hay entre los empleados y 

SELECT E.FirstName AS EmployeeName, S.FirstName AS SupervisorName
FROM Employee E
LEFT JOIN Employee S 
	ON E.ReportsTo = S.EmployeeId 
ORDER BY E.BirthDate DESC 
LIMIT 15;