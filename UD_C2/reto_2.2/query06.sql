SELECT *
FROM Track
WHERE TrackId IN (
	SELECT TrackId
    FROM InvoiceLine
    WHERE InvoiceId IN (
		SELECT InvoiceId
        FROM Invoice
        WHERE CustomerId IN (
			SELECT CustomerId
            FROM Customer
            WHERE FirstName = "Luis" AND LastName = "Rojas"
        )
    )
);