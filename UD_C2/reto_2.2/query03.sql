SELECT CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Employee
WHERE EmployeeId = (
	SELECT DISTINCT SupportRepId 
    FROM Customer
    WHERE Customer.SupportRepId = Employee.EmployeeId
);

-- CORRECIÓN CLASE:
SELECT CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Employee
WHERE EmployeeId IN (
	SELECT DISTINCT SupportRepId 
    FROM Customer
    WHERE Customer.SupportRepId
);

-- OTRA FORMA - CON EXISTS (CORRECIÓN CLASE):
SELECT CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Employee
WHERE EXISTS (
	SELECT *
    FROM Customer
    WHERE SupportRepId = EmployeeId
);