SELECT *
FROM Track AS T
WHERE T.Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
);