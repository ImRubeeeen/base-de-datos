SELECT * 
FROM Track
WHERE TrackId NOT IN (
	SELECT TrackId 
    FROM InvoiceLine
);

-- PASO A PASO (el paso a paso hecho en clase):
	-- Lista de canciones que sí se han comprado

SELECT TrackId
FROM InvoiceLine;

	-- Detalles de las canciones que sí se han comprado
    
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId0 IN (
	SELECT TrackId
	FROM InvoiceLine
);

	-- Canciones que no se han comprado
    
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId NOT IN (
	SELECT TrackId
	FROM InvoiceLine
);