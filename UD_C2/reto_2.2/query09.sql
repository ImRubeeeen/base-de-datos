SELECT C.CustomerId, CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Customer AS C
WHERE EXISTS (
    SELECT 1
    FROM Invoice AS I
    JOIN InvoiceLine AS IL ON I.InvoiceId = IL.InvoiceId
    WHERE I.CustomerId = C.CustomerId
    GROUP BY I.InvoiceId
    HAVING COUNT(IL.TrackId) > 10
);