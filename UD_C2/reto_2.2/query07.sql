-- Suma la duración de todas las canciones de cada album y se agrupan por 'AlbumId' y 'Title' de la tabla "Album"
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title;

-- Resultado final: La consulta principal es igual, pero añadiendo el HAVING para filtrar solo que la suma sea mayor que el de la subconsulta
-- que se encarga de calcular la duración media 
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING SUM(T.Milliseconds) > (
    SELECT AVG(DuracionTotal) 
    FROM (
        SELECT SUM(Milliseconds) AS DuracionTotal
        FROM Track
        GROUP BY AlbumId
    ) AS Suma_Canciones
);