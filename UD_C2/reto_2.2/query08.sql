-- Lo que hace esta consultada es que sie encuentra 1 cliente que haya comprado TODOS los álbumes de Queen, sale en el resultado final.
-- En este caso, no hay nadie que haya comprado todos.

SELECT C.CustomerId, C.FirstName, C.LastName
FROM Customer AS C
WHERE NOT EXISTS (
    SELECT 1
    FROM Album AS A
    JOIN Artist AS AR ON A.ArtistId = AR.ArtistId
    WHERE AR.Name = 'Queen'
    AND NOT EXISTS (
        SELECT 1
        FROM InvoiceLine AS IL
        JOIN Invoice AS I ON IL.InvoiceId = I.InvoiceId
        JOIN Track AS T ON IL.TrackId = T.TrackId
        WHERE T.AlbumId = A.AlbumId
        AND I.CustomerId = C.CustomerId
    )
);