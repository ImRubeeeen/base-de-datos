SELECT 
    TrackName,
    TotalCompradas
FROM
    (
        SELECT
            T.Name AS TrackName,
            SUM(IL.Quantity) AS TotalCompradas
        FROM
            InvoiceLine AS IL
        INNER JOIN
            Track AS T ON IL.TrackId = T.TrackId
        GROUP BY
            T.TrackId,
            T.Name
    ) AS CancionesCompradas
ORDER BY
    TotalCompradas DESC
LIMIT 10;