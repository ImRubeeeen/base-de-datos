SELECT C.CustomerId, CONCAT(FirstName, " ", LastName) AS Nombre_Completo,
       CASE 
           WHEN SUM(I.Total) > 45 THEN 'VIP'
           ELSE 'Regular'
       END AS CustomerType
FROM Customer AS C
JOIN Invoice AS I ON C.CustomerId = I.CustomerId
GROUP BY C.CustomerId, Nombre_Completo;