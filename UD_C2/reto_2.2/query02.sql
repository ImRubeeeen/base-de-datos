-- En esta query lo que hago es filtrar por el ArtistId de la tabla Artist y con 2 subconsultas es relacionar la tabla de Album con Track para sacar el tiempo
-- y así  de esta forma poder sacar los artistas que tienen canciones con duración superior a 5 minutos.

SELECT AR.Name
FROM Artist AS AR
WHERE AR.ArtistId IN (
    SELECT A.ArtistId
    FROM Album AS A
    WHERE A.AlbumId IN (
        SELECT T.AlbumId
        FROM Track AS T
        WHERE T.Milliseconds > 300000
    )
);