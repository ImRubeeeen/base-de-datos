SELECT
    P.Name AS PlaylistName,
    T.Name AS TrackName,
    A.Title AS AlbumTitle,
    T.UnitPrice AS Price
FROM
    Playlist AS P
JOIN
    PlaylistTrack AS PT ON P.PlaylistId = PT.PlaylistId
JOIN
    Track AS T ON PT.TrackId = T.TrackId
JOIN
    Album AS A ON T.AlbumId = A.AlbumId
WHERE
    P.Name LIKE 'M%'
    AND (
        SELECT
            COUNT(*)
        FROM
            PlaylistTrack AS PT2
        JOIN
            Track AS T2 ON PT2.TrackId = T2.TrackId
        WHERE
            PT2.PlaylistId = P.PlaylistId
            AND T2.AlbumId = T.AlbumId
            AND T2.UnitPrice <= T.UnitPrice
    ) <= 3
ORDER BY
    A.Title ASC,
    T.UnitPrice ASC;