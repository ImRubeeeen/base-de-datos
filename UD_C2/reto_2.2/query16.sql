SELECT EmployeeId, CONCAT(FirstName, " ", LastName) AS Nombre_Completo, Title,
       CASE
           WHEN Title = 'Manager' THEN 'Nivel 1'
           WHEN Title = 'Assistant' THEN 'Nivel 2'
           ELSE 'Nivel 3'
       END AS Nivel_Empleado
FROM Employee;