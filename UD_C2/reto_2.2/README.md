# Reto 2.2: Consultas con subconsultas, subconltas y funciones de agregación, y CASE.

Rubén Martí Garcia.

En este reto trabajamos con la base de datos `chinook`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

## Query 1
Muestra las listas de reproducción cuyo nombre comienza por M, junto a las 3 primeras canciones de cada uno, ordenadas por álbum y por precio (más bajo primero).

```sql
SELECT
    P.Name AS PlaylistName,
    T.Name AS TrackName,
    A.Title AS AlbumTitle,
    T.UnitPrice AS Price
FROM
    Playlist AS P
JOIN
    PlaylistTrack AS PT ON P.PlaylistId = PT.PlaylistId
JOIN
    Track AS T ON PT.TrackId = T.TrackId
JOIN
    Album AS A ON T.AlbumId = A.AlbumId
WHERE
    P.Name LIKE 'M%'
    AND (
        SELECT
            COUNT(*)
        FROM
            PlaylistTrack AS PT2
        JOIN
            Track AS T2 ON PT2.TrackId = T2.TrackId
        WHERE
            PT2.PlaylistId = P.PlaylistId
            AND T2.AlbumId = T.AlbumId
            AND T2.UnitPrice <= T.UnitPrice
    ) <= 3
ORDER BY
    A.Title ASC,
    T.UnitPrice ASC;
```

## Query 2
Muestra todos los artistas que tienen canciones con duración superior a 5 minutos.

```sql
-- En esta query lo que hago es filtrar por el ArtistId de la tabla Artist y con 2 subconsultas es relacionar la tabla de Album con Track para sacar el tiempo
-- y así  de esta forma poder sacar los artistas que tienen canciones con duración superior a 5 minutos.

SELECT AR.Name
FROM Artist AS AR
WHERE AR.ArtistId IN (
    SELECT A.ArtistId
    FROM Album AS A
    WHERE A.AlbumId IN (
        SELECT T.AlbumId
        FROM Track AS T
        WHERE T.Milliseconds > 300000
    )
);
```

## Query 3
Muestra nombre y apellidos de los empleados que tienen clientes asignados. (Pista: se puede hacer con EXISTS).

```sql
SELECT CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Employee
WHERE EmployeeId = (
	SELECT DISTINCT SupportRepId 
    FROM Customer
    WHERE Customer.SupportRepId = Employee.EmployeeId
);

-- CORRECIÓN CLASE:
SELECT CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Employee
WHERE EmployeeId IN (
	SELECT DISTINCT SupportRepId 
    FROM Customer
    WHERE Customer.SupportRepId
);

-- OTRA FORMA - CON EXISTS (CORRECIÓN CLASE):
SELECT CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Employee
WHERE EXISTS (
	SELECT *
    FROM Customer
    WHERE SupportRepId = EmployeeId
);
```

## Query 4
Muestra todas las canciones que no han sido compradas.

```sql
SELECT * 
FROM Track
WHERE TrackId NOT IN (
	SELECT TrackId 
    FROM InvoiceLine
);

-- PASO A PASO (el paso a paso hecho en clase):
	-- Lista de canciones que sí se han comprado

SELECT TrackId
FROM InvoiceLine;

	-- Detalles de las canciones que sí se han comprado
    
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId0 IN (
	SELECT TrackId
	FROM InvoiceLine
);

	-- Canciones que no se han comprado
    
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId NOT IN (
	SELECT TrackId
	FROM InvoiceLine
);
```

## Query 5
Lista los empleados junto a sus subordinados (empleados que reportan a ellos).

```sql
-- Con JOIN:
SELECT
    E1.FirstName AS SupervisorFirstName,
    E1.LastName AS SupervisorLastName,
    E2.FirstName AS SubordinateFirstName,
    E2.LastName AS SubordinateLastName
FROM
    Employee AS E1
LEFT JOIN
    Employee AS E2 ON E1.EmployeeId = E2.ReportsTo
ORDER BY
    E1.LastName,
    E1.FirstName,
    E2.LastName,
    E2.FirstName;

-- Con subconsultas:
SELECT 
    Supervisor.FirstName AS SupervisorFirstName,
    Supervisor.LastName AS SupervisorLastName,
    Subordinate.FirstName AS SubordinateFirstName,
    Subordinate.LastName AS SubordinateLastName
FROM
    Employee AS Supervisor
LEFT JOIN 
    (
        SELECT
            E2.EmployeeId,
            E2.FirstName,
            E2.LastName,
            E2.ReportsTo
        FROM
            Employee AS E2
    ) AS Subordinate ON Supervisor.EmployeeId = Subordinate.ReportsTo
ORDER BY
    Supervisor.LastName,
    Supervisor.FirstName,
    Subordinate.LastName,
    Subordinate.FirstName;
```

## Query 6
Muestra todas las canciones que ha comprado el cliente Luis Rojas.

```sql
SELECT *
FROM Track
WHERE TrackId IN (
	SELECT TrackId
    FROM InvoiceLine
    WHERE InvoiceId IN (
		SELECT InvoiceId
        FROM Invoice
        WHERE CustomerId IN (
			SELECT CustomerId
            FROM Customer
            WHERE FirstName = "Luis" AND LastName = "Rojas"
        )
    )
);
```

## Query 7
Canciones que son más caras que cualquier otra canción del mismo álbum.

```sql
-- Suma la duración de todas las canciones de cada album y se agrupan por 'AlbumId' y 'Title' de la tabla "Album"
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title;

-- Resultado final: La consulta principal es igual, pero añadiendo el HAVING para filtrar solo que la suma sea mayor que el de la subconsulta
-- que se encarga de calcular la duración media 
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING SUM(T.Milliseconds) > (
    SELECT AVG(DuracionTotal) 
    FROM (
        SELECT SUM(Milliseconds) AS DuracionTotal
        FROM Track
        GROUP BY AlbumId
    ) AS Suma_Canciones
);
```

## Query 8
Clientes que han comprado todos los álbumes de un artista específico.

```sql
-- Lo que hace esta consultada es que sie encuentra 1 cliente que haya comprado TODOS los álbumes de Queen, sale en el resultado final.
-- En este caso, no hay nadie que haya comprado todos.

SELECT C.CustomerId, C.FirstName, C.LastName
FROM Customer AS C
WHERE NOT EXISTS (
    SELECT 1
    FROM Album AS A
    JOIN Artist AS AR ON A.ArtistId = AR.ArtistId
    WHERE AR.Name = 'Queen'
    AND NOT EXISTS (
        SELECT 1
        FROM InvoiceLine AS IL
        JOIN Invoice AS I ON IL.InvoiceId = I.InvoiceId
        JOIN Track AS T ON IL.TrackId = T.TrackId
        WHERE T.AlbumId = A.AlbumId
        AND I.CustomerId = C.CustomerId
    )
);
```

## Query 9
Clientes que han comprado más de 20 canciones en una sola transacción.

```sql
SELECT C.CustomerId, CONCAT(FirstName, " ", LastName) AS Nombre_Completo
FROM Customer AS C
WHERE EXISTS (
    SELECT 1
    FROM Invoice AS I
    JOIN InvoiceLine AS IL ON I.InvoiceId = IL.InvoiceId
    WHERE I.CustomerId = C.CustomerId
    GROUP BY I.InvoiceId
    HAVING COUNT(IL.TrackId) > 10
);
```

## Query 10
Muestra las 10 canciones más compradas.

```sql
SELECT 
    TrackName,
    TotalCompradas
FROM
    (
        SELECT
            T.Name AS TrackName,
            SUM(IL.Quantity) AS TotalCompradas
        FROM
            InvoiceLine AS IL
        INNER JOIN
            Track AS T ON IL.TrackId = T.TrackId
        GROUP BY
            T.TrackId,
            T.Name
    ) AS CancionesCompradas
ORDER BY
    TotalCompradas DESC
LIMIT 10;
```

## Query 11
Muestra las canciones con una duración superior a la media.

```sql
SELECT *
FROM Track AS T
WHERE T.Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
);
```

## Query 12
Para demostrar lo bueno que es nuestro servicio, muestra el número de países donde tenemos clientes, el número de géneros músicales de los que disponemos y el número de pistas.

```sql
SELECT 
    (SELECT COUNT(DISTINCT Country) FROM Customer) AS "Países en el que operamos",
    (SELECT COUNT(DISTINCT GenreId) FROM Genre) AS Géneros,
    (SELECT COUNT(*) FROM Track) AS Pistas
```

## Query 13
Canciones vendidas más que la media de ventas por canción.

```sql

```

## Query 14
Clasifica a los clientes en tres categorías según su país: “Local” si el país es ‘USA’, “Internacional” si el país es distinto de ‘USA’, y “Desconocido” si el país es nulo.

```sql
SELECT CustomerId, CONCAT(FirstName, " " , LastName) AS Nombre_Completo,
       CASE
           WHEN Country = 'USA' THEN 'Local'
           WHEN Country IS NOT NULL THEN 'Internacional'
           ELSE 'Desconocido'
       END AS Categoria_Cliente
FROM customer;
```

## Query 15
Calcula el descuento aplicado sobre cada factura, que depende del monto total de la factura (10% para facturas superiores a $100).

```sql
SELECT InvoiceId, Total,
       CASE
           WHEN Total > 100 THEN Total * 0.10
           ELSE 0
       END AS Descuento
FROM Invoice;
```

## Query 16
Clasifica a los empleados en diferentes niveles según su cargo (manager, asistente o empleado).

```sql
SELECT EmployeeId, CONCAT(FirstName, " ", LastName) AS Nombre_Completo, Title,
       CASE
           WHEN Title = 'Manager' THEN 'Nivel 1'
           WHEN Title = 'Assistant' THEN 'Nivel 2'
           ELSE 'Nivel 3'
       END AS Nivel_Empleado
FROM Employee;
```

## Query 17
Etiqueta a los clientes como “VIP” si han gastado más de $500 en compras totales.

```sql
SELECT C.CustomerId, CONCAT(FirstName, " ", LastName) AS Nombre_Completo,
       CASE 
           WHEN SUM(I.Total) > 45 THEN 'VIP'
           ELSE 'Regular'
       END AS CustomerType
FROM Customer AS C
JOIN Invoice AS I ON C.CustomerId = I.CustomerId
GROUP BY C.CustomerId, Nombre_Completo;
```