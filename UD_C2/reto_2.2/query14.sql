SELECT CustomerId, CONCAT(FirstName, " " , LastName) AS Nombre_Completo,
       CASE
           WHEN Country = 'USA' THEN 'Local'
           WHEN Country IS NOT NULL THEN 'Internacional'
           ELSE 'Desconocido'
       END AS Categoria_Cliente
FROM customer;