SELECT 
    (SELECT COUNT(DISTINCT Country) FROM Customer) AS "Países en el que operamos",
    (SELECT COUNT(DISTINCT GenreId) FROM Genre) AS Géneros,
    (SELECT COUNT(*) FROM Track) AS Pistas