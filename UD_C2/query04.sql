SELECT A.AlbumId, A.Title, COUNT(T.TrackId) AS Numero_canciones
FROM Album A
JOIN Track T 
ON A.AlbumId = T.AlbumId
WHERE A.AlbumId IN (
    SELECT AlbumId
    FROM Track
)
GROUP BY A.AlbumId
ORDER BY Numero_canciones DESC;