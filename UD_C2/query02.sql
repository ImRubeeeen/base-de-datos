-- Selecciono el número de canciones:

SELECT COUNT(T.TrackId) AS Num_Canciones
FROM Track T;

-- Selecciono el número de canciones agrupando por AlbumId de tracks:

SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
FROM Track T
GROUP BY T.AlbumId;

-- Calculo la media del número de canciones

SELECT AVG (Num_Canciones)
FROM (
	SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
	FROM Track T
	GROUP BY T.AlbumId
) Ntrack;

-- Resultado final:

SELECT AlbumId, COUNT(*) AS n_tracks
FROM Track
GROUP BY AlbumId
HAVING n_tracks > (
	SELECT AVG (Num_Canciones)
	FROM (
		SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
		FROM Track T
		GROUP BY T.AlbumId
	) Ntrack
);