SELECT TrackId, Name, Milliseconds
FROM Track AS T
WHERE T.Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
)
ORDER BY Milliseconds ASC;