SELECT 
    AR.Name AS 'Nombre del Artista',
    (SELECT AL.Title
     FROM Album AS AL
     WHERE AL.ArtistId = AR.ArtistId
     ORDER BY AL.AlbumId DESC
     LIMIT 1
	) AS 'Álbum Más Reciente'
FROM Artist AS AR;
