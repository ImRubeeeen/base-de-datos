-- Busco la id de Reggae
SELECT GenreId
FROM Genre
WHERE Name = 'Reggae';

-- Selecciono la PlaylistId cuyo GenreId contenga el nombre de Reggae
SELECT *
FROM Playlist
WHERE PlaylistId IN (
	SELECT GenreId
    FROM Genre
    WHERE Name = 'Reggae'
);