-- Selecciono el 'AlbumId' y el 'Title' de "Album" y hago una subconsulta donde selecciono el COUNT de 'TrackId' (cuenta la id de las canciones) de "Track"
-- ahora que cuenta la id de las canciones filtro con el 'AlbumId' de "Track" y "Album"
SELECT 
	A.AlbumId,
    A.Title,
    (SELECT COUNT(T.TrackId) 
     FROM Track AS T 
     WHERE T.AlbumId = A.AlbumId) AS 'Número de Canciones'
FROM Album AS A;
