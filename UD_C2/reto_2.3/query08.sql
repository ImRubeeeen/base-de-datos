-- Selecciono todo de "Track" y hago una subconsulta con un JOIN. Dentro de ella selecciono 'GenreId'de "Track", agrupandolo por el id del género
-- ordenandolo con el número de veces cada vez que aparece el 'GenreId' de mayor a menor limitandolo a 1 para que solo devuelva el id que sale más veces
SELECT *
FROM Track AS T
JOIN (
    SELECT GenreId
    FROM Track
    GROUP BY GenreId
    ORDER BY COUNT(*) DESC
    LIMIT 1
) AS g_masCanciones ON T.GenreId = g_masCanciones.GenreId;