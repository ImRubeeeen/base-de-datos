-- Me voy a quedar con el TrackId, Name y Milliseconds de la tabla Track. Si los millisegundos es mayor que a la media (de ahí lo de seleccionar la media "AVG") saldrá en  la tabla
-- ordenado por Milliseconds de forma ascendente
SELECT TrackId, Name, Milliseconds
FROM Track AS T
WHERE T.Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
)
ORDER BY Milliseconds ASC;