-- Selecciono el 'EmployeeId', 'FirstName' y el 'LastName' de "Employee". Hago la subconsulta que se se encarga de sumar el 'Total' de "Invoice" 
-- y hago el INNER JOIN para que la subcon sulta pueda acceder a ambos datos de "Invoice" y "Customer". Finalmente filtro por las ids de SupportRepId y
-- EmployeeId ya que se relacionan entre sí.
SELECT
	E.EmployeeId,
    E.FirstName,
    E.LastName,
    (SELECT SUM(i.Total) 
     FROM Invoice AS I 
     INNER JOIN Customer AS C ON I.CustomerId = C.CustomerId 
     WHERE C.SupportRepId = E.EmployeeId
     ) AS VentasTotales
FROM Employee AS E
ORDER BY VentasTotales DESC;