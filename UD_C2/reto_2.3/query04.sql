-- Selecciono el 'CustomerId' de la tabla "Invoice" cuyo total es mayor a 20
SELECT CustomerId
FROM Invoice
WHERE Total > 20;

-- Selecciono el 'CustomerId', 'FirstName' y 'LastName' de la tabla "Customer" y si tienen el mismo 'CustomerId' las tablas de "Customer" e "Invoice"
-- y hago la subconsulta que he hecho en el paso anterior.
SELECT CustomerId, FirstName, LastName
FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId
    FROM Invoice
    WHERE Total > 20
);