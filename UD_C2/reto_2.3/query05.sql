SELECT 
    A.Title AS AlbumTitle,
    AR.Name AS ArtistName,
    Num_Canciones
FROM
    Album AS A
INNER JOIN
    Artist AS AR ON A.ArtistId = AR.ArtistId
INNER JOIN
    (
        SELECT 
            AlbumId,
            COUNT(*) AS Num_Canciones
        FROM Track
        GROUP BY AlbumId
        HAVING COUNT(*) > 15
    ) AS Canciones_Album ON A.AlbumId = Canciones_Album.AlbumId;