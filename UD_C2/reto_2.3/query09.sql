SELECT 
    T.Name AS TrackName
FROM PlaylistTrack AS PT
INNER JOIN Track AS T ON PT.TrackId = T.TrackId
WHERE PT.PlaylistId = (
	SELECT PlaylistId
	FROM
		(
			SELECT
				PlaylistId,
				COUNT(*) AS Num_Canciones
			FROM PlaylistTrack
			GROUP BY PlaylistId
			ORDER BY Num_Canciones DESC
			LIMIT 1
		) AS PlaylistMaxCanciones
	);