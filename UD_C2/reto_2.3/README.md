# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Rubén Martí Garcia

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1
Obtener las canciones con una duración superior a la media.

```sql
-- Me voy a quedar con el TrackId, Name y Milliseconds de la tabla Track. Si los millisegundos es mayor que a la media (de ahí lo de seleccionar la media "AVG") saldrá en  la tabla
-- ordenado por Milliseconds de forma ascendente
SELECT TrackId, Name, Milliseconds
FROM Track AS T
WHERE T.Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
)
ORDER BY Milliseconds ASC;
```
### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

```sql
-- Selecciono todo de la tabla Invoice, filtro por CustomerId y busca en la tabla "Customer" y obtiene el 'CustomerId' asociado con el correo electrónico “emma_jones@hotmail.com”
-- y ordeno por InvoiceDate de forma descendente con un limite de 5 para quedarme con lass primeras 5 facturas.
SELECT *
FROM Invoice
WHERE CustomerId IN (
    SELECT CustomerId
    FROM Customer
    WHERE Email = "emma_jones@hotmail.com"
)
ORDER BY InvoiceDate DESC
LIMIT 5;  
```

## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.

```sql
-- Busco la id de Reggae
SELECT GenreId
FROM Genre
WHERE Name = 'Reggae';

-- Selecciono la PlaylistId cuyo GenreId contenga el nombre de Reggae
SELECT *
FROM Playlist
WHERE PlaylistId IN (
	SELECT GenreId
    FROM Genre
    WHERE Name = 'Reggae'
);
```

### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.

```sql
-- Selecciono el 'CustomerId' de la tabla "Invoice" cuyo total es mayor a 20
SELECT CustomerId
FROM Invoice
WHERE Total > 20;

-- Selecciono el 'CustomerId', 'FirstName' y 'LastName' de la tabla "Customer" y si tienen el mismo 'CustomerId' las tablas de "Customer" e "Invoice"
-- y hago la subconsulta que he hecho en el paso anterior.
SELECT CustomerId, FirstName, LastName
FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId
    FROM Invoice
    WHERE Total > 20
);
```

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.

```sql
SELECT 
    A.Title AS AlbumTitle,
    AR.Name AS ArtistName,
    Num_Canciones
FROM
    Album AS A
INNER JOIN
    Artist AS AR ON A.ArtistId = AR.ArtistId
INNER JOIN
    (
        SELECT 
            AlbumId,
            COUNT(*) AS Num_Canciones
        FROM Track
        GROUP BY AlbumId
        HAVING COUNT(*) > 15
    ) AS Canciones_Album ON A.AlbumId = Canciones_Album.AlbumId;
```

### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.

```sql
-- Selecciono los álbumes que tienen un número de pistas superior al promedio de todas las pistas por álbum en la base de datos, y los ordena de menor a mayor según su número de pistas
SELECT AlbumId, COUNT(*) AS n_tracks
FROM Track
GROUP BY AlbumId
HAVING n_tracks > (
	SELECT AVG (Num_Canciones)
	FROM (
		SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
		FROM Track T
		GROUP BY T.AlbumId
	) Ntrack
)
ORDER BY n_tracks ASC;
```

### Consulta 7
Obtener los álbumes con una duración total superior a la media.

```sql
-- Suma la duración de todas las canciones de cada album y se agrupan por 'AlbumId' y 'Title' de la tabla "Album"
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title;

-- Resultado final: La consulta principal es igual, pero añadiendo el HAVING para filtrar solo que la suma sea mayor que el de la subconsulta
-- que se encarga de calcular la duración media 
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING SUM(T.Milliseconds) > (
    SELECT AVG(DuracionTotal) 
    FROM (
        SELECT SUM(Milliseconds) AS DuracionTotal
        FROM Track
        GROUP BY AlbumId
    ) AS Suma_Canciones
);
```

### Consulta 8
Canciones del género con más canciones.

```sql
-- Selecciono todo de "Track" y hago una subconsulta con un JOIN. Dentro de ella selecciono 'GenreId'de "Track", agrupandolo por el id del género
-- ordenandolo con el número de veces cada vez que aparece el 'GenreId' de mayor a menor limitandolo a 1 para que solo devuelva el id que sale más veces
SELECT *
FROM Track AS T
JOIN (
    SELECT GenreId
    FROM Track
    GROUP BY GenreId
    ORDER BY COUNT(*) DESC
    LIMIT 1
) AS g_masCanciones ON T.GenreId = g_masCanciones.GenreId;
```

### Consulta 9
Canciones de la _playlist_ con más canciones.

```sql
SELECT 
    T.Name AS TrackName
FROM PlaylistTrack AS PT
INNER JOIN Track AS T ON PT.TrackId = T.TrackId
WHERE PT.PlaylistId = (
	SELECT PlaylistId
	FROM
		(
			SELECT
				PlaylistId,
				COUNT(*) AS Num_Canciones
			FROM PlaylistTrack
			GROUP BY PlaylistId
			ORDER BY Num_Canciones DESC
			LIMIT 1
		) AS PlaylistMaxCanciones
	);
```


## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

```sql
SELECT C.FirstName, C.LastName, 
(
    SELECT SUM(I.Total)
    FROM Invoice I
    WHERE I.CustomerId = C.CustomerId
) AS TotalGastado
FROM Customer C
ORDER BY TotalGastado DESC;
```

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

```sql
-- Selecciono el 'EmployeeId', 'FirstName' y el 'LastName' de "Employee". Con la subconsulta lo que hago es contar el número de clientes
SELECT E.EmployeeId, E.FirstName, E.LastName, 
(
    SELECT COUNT(*)
    FROM Customer C
    WHERE C.SupportRepId = E.EmployeeId
) AS NumeroDeClientes
FROM Employee E
ORDER BY NumeroDeClientes DESC;
```

### Consulta 12
Ventas totales de cada empleado.

```sql
-- Selecciono el 'EmployeeId', 'FirstName' y el 'LastName' de "Employee". Hago la subconsulta que se se encarga de sumar el 'Total' de "Invoice" 
-- y hago el INNER JOIN para que la subcon sulta pueda acceder a ambos datos de "Invoice" y "Customer". Finalmente filtro por las ids de SupportRepId y
-- EmployeeId ya que se relacionan entre sí.
SELECT
	E.EmployeeId,
    E.FirstName,
    E.LastName,
    (SELECT SUM(i.Total) 
     FROM Invoice AS I 
     INNER JOIN Customer AS C ON I.CustomerId = C.CustomerId 
     WHERE C.SupportRepId = E.EmployeeId
     ) AS VentasTotales
FROM Employee AS E
ORDER BY VentasTotales DESC;
```

### Consulta 13
Álbumes junto al número de canciones en cada uno.

```sql
-- Selecciono el 'AlbumId' y el 'Title' de "Album" y hago una subconsulta donde selecciono el COUNT de 'TrackId' (cuenta la id de las canciones) de "Track"
-- ahora que cuenta la id de las canciones filtro con el 'AlbumId' de "Track" y "Album"
SELECT 
	A.AlbumId,
    A.Title,
    (SELECT COUNT(T.TrackId) 
     FROM Track AS T 
     WHERE T.AlbumId = A.AlbumId) AS 'Número de Canciones'
FROM Album AS A;
```

### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

```sql
SELECT
    AR.Name AS 'Nombre del Artista',
    (SELECT AL.Title
     FROM Album AS AL
     WHERE AL.ArtistId = AR.ArtistId
     ORDER BY AL.AlbumId DESC
     LIMIT 1
	) AS 'Álbum Más Reciente'
FROM Artist AS AR;
```


## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/


<!--
HAVING ??
GROUP BY ??
-->
