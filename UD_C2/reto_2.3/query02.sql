-- Selecciono todo de la tabla Invoice, filtro por CustomerId y busca en la tabla "Customer" y obtiene el 'CustomerId' asociado con el correo electrónico “emma_jones@hotmail.com”
-- y ordeno por InvoiceDate de forma descendente con un limite de 5 para quedarme con lass primeras 5 facturas.
SELECT *
FROM Invoice
WHERE CustomerId IN (
	SELECT CustomerId
	FROM customer
	WHERE Email = "emma_jones@hotmail.com"
)
ORDER BY InvoiceDate DESC
LIMIT 5; 