-- Selecciono los álbumes que tienen un número de pistas superior al promedio de todas las pistas por álbum en la base de datos, y los ordena de menor a mayor según su número de pistas
SELECT AlbumId, COUNT(*) AS n_tracks
FROM Track
GROUP BY AlbumId
HAVING n_tracks > (
	SELECT AVG (Num_Canciones)
	FROM (
		SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
		FROM Track T
		GROUP BY T.AlbumId
	) Ntrack
)
ORDER BY n_tracks ASC;