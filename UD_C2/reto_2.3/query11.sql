-- Selecciono el 'EmployeeId', 'FirstName' y el 'LastName' de "Employee". Con la subconsulta lo que hago es contar el número de clientes
SELECT E.EmployeeId, E.FirstName, E.LastName, 
(
    SELECT COUNT(*)
    FROM Customer C
    WHERE C.SupportRepId = E.EmployeeId
) AS NumeroDeClientes
FROM Employee E
ORDER BY NumeroDeClientes DESC;