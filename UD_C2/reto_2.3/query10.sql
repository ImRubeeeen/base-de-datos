SELECT C.FirstName, C.LastName, 
(
    SELECT SUM(I.Total)
    FROM Invoice I
    WHERE I.CustomerId = C.CustomerId
) AS TotalGastado
FROM Customer C
ORDER BY TotalGastado DESC;