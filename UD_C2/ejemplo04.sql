SELECT
	COUNT(*),
	AVG(Milliseconds)/1000 AS "Media (s)",
    MAX(Milliseconds)/1000/60 AS "Max (m)"
FROM Track;