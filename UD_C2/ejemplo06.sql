SELECT
	T.Name,
    T.Composer,
    T.Milliseconds,
    T.Bytes,
    T.UnitPrice,
    A.Title AS Album,
    G.Name AS Genero
FROM Track AS T
JOIN Album AS A
	ON T.AlbumId = A.AlbumId
JOIN Genre AS G
	ON T.GenreId = G.GenreId;

-- ANALÍSIS: Duración media por género
SELECT
	G.Name AS Genero,
    A.Title AS Album,
    AVG(T.Milliseconds/1000/60) AS "Duración (m)",
    COUNT(T.Name),
    COUNT(CASE WHEN T.Name LIKE "A%" THEN 1 END)
FROM Track AS T
JOIN Album AS A
	ON T.AlbumId = A.AlbumId
JOIN Genre AS G
	ON T.GenreId = G.GenreId
GROUP BY G.Name, A.AlbumId
ORDER BY A.AlbumId;