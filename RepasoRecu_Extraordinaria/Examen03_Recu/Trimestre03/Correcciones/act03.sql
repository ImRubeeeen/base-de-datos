SET ROLE 'Chinook Admin', Profesor;
SHOW GRANTS;

SELECT * FROM Chinook.Customer;

SELECT 
	CASE
		WHEN Customer.email LIKE "%@ucm.com" THEN CONCAT(alumno.nombre, ' ', apellido1, ' ', apellido2)
        ELSE CONCAT(FirstName, ' ', LastName)
	END AS "Nombre Completo", 
	Customer.Email AS 'email', 
    asignatura.nombre AS Asignatura
FROM Chinook.Customer
LEFT JOIN alumno ON alumno.email = Customer.email
LEFT JOIN matricula USING (idAlumno)
LEFT JOIN asignatura USING (idAsignatura);