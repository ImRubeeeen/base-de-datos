-- Apartado A
CREATE USER 'Luz'@'localhost' IDENTIFIED BY 'jardy';
CREATE USER 'Luz'@'78.58.2.2' IDENTIFIED BY 'jardy';

GRANT ALL PRIVILEGES ON jardyPub.* TO 'Luz'@'localhost';
GRANT ALL PRIVILEGES ON jardyPub.* TO 'Luz'@'78.58.2.2';
GRANT ALL PRIVILEGES ON jardyPriv.* TO 'Luz'@'localhost';
GRANT ALL PRIVILEGES ON jardyPriv.* TO 'Luz'@'78.58.2.2';
FLUSH PRIVILEGES;

SHOW GRANTS FOR 'Luz'@'localhost';
SHOW GRANTS FOR 'Luz'@'78.58.2.2';

-- Apartado B (En este caso, si no pones el @ para definir el host es lo mismo que el %)
CREATE USER 'Eliseo'@'%' IDENTIFIED BY 'jardy';
CREATE USER 'Marcos'@'%' IDENTIFIED BY 'jardy';
CREATE USER 'Gala'@'%' IDENTIFIED BY 'jardy';

GRANT SELECT ON jardyPriv.* TO 'Eliseo'@'%';
GRANT SELECT ON jardyPriv.* TO 'Marcos'@'%';
GRANT SELECT ON jardyPriv.* TO 'Gala'@'%';
GRANT SELECT ON jardyPub.textos TO 'Eliseo'@'%';
GRANT SELECT ON jardyPub.textos TO 'Marcos'@'%';
GRANT SELECT ON jardyPub.textos TO 'Gala'@'%';
FLUSH PRIVILEGES;

SHOW GRANTS FOR 'Eliseo'@'%';
SHOW GRANTS FOR 'Marcos'@'%';
SHOW GRANTS FOR 'Gala'@'%';

-- Apartado C
CREATE USER 'web21'@'58.58.2.1' IDENTIFIED BY 'jardy';
CREATE USER 'web22'@'58.58.2.2' IDENTIFIED BY 'jardy';
CREATE USER 'web23'@'58.58.2.3' IDENTIFIED BY 'jardy';

GRANT SELECT ON *.* TO 'web21'@'58.58.2.1';
GRANT SELECT ON *.* TO 'web22'@'58.58.2.2';
GRANT SELECT ON *.* TO 'web23'@'58.58.2.3';
FLUSH PRIVILEGES;

SHOW GRANTS FOR 'web21'@'58.58.2.1';
SHOW GRANTS FOR 'web22'@'58.58.2.2';
SHOW GRANTS FOR 'web23'@'58.58.2.3';