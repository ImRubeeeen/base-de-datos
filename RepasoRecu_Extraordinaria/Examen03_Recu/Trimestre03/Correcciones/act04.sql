SELECT 
	(
		SELECT CONCAT(alumno.nombre, ' ',  apellido1, ' ', apellido2) FROM alumno
		WHERE idAlumno = matricula.idAlumno
	) AS "Nombre matricula", nota
FROM matricula
WHERE idAsignatura = 
	(
		SELECT idAsignatura FROM asignatura 
		WHERE nombre = "Almacenamiento e integración de datos"
	)
AND nota > (
	SELECT AVG(nota)
	FROM matricula
	GROUP BY idAsignatura
	HAVING idAsignatura = (
		SELECT idAsignatura 
		FROM asignatura WHERE nombre = "Almacenamiento e integración de datos"
	)
)
ORDER BY nota DESC;