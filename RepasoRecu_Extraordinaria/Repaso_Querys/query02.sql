-- Listar las facturas del cliente cuyo email es “emma_jones@hotmail.com”.

-- Seleccciono el id de la tabla de "Customer" y le digo que el email sea igual al de "emma_jones@hotmail.com"
SELECT CustomerId
FROM Customer
WHERE Email = "emma_jones@hotmail.com";

-- Me quedo con las facturas asociados al id sacada en la consulta anterior
SELECT *
FROM Invoice
WHERE CustomerId IN (
	SELECT CustomerId
	FROM Customer
	WHERE Email = "emma_jones@hotmail.com"
)
ORDER BY InvoiceDate DESC
LIMIT 5;
