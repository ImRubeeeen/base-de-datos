-- Obtener la información de los clientes que han realizado compras superiores a 20€.

-- Miro los clientes que hay
SELECT *
FROM Customer;

-- Saco las facturas con un total que es superior a 20
SELECT *
FROM Invoice
WHERE Total > 20;

-- Completo, uniendo la primera parte, con la segunda
SELECT *
FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId
	FROM Invoice
	WHERE Total > 20
);