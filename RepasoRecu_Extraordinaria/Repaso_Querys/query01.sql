-- Obtener las canciones con una duración superior a la media.

-- Miro la media de las canciones
SELECT AVG (Milliseconds)
FROM Track;

--  Aquí selecciono todo de la tabla de track y el filtro es que si los millisegundos es mayor que y ahí uno la consulta que hice anteriormente para calcular la media de los tracks
SELECT *
FROM Track AS T
WHERE Milliseconds > (
	SELECT AVG (Milliseconds)
	FROM Track
)
ORDER BY Milliseconds DESC;