-- Obtener los álbumes con una duración total superior a la media.

SELECT *
FROM Album;

-- Duración total de cada álbum
SELECT A.AlbumId, SUM(Milliseconds) AS Duracion_Total
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId;

-- Media
SELECT AVG(T.Milliseconds)
FROM Track T
GROUP BY T.AlbumId;

-- Media de la duración de todos los álbumes
SELECT A.AlbumId, SUM(Milliseconds) AS Duracion_Total
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
HAVING SUM(T.Milliseconds) > (
	SELECT AVG(Duracion_Total)
	FROM Track T
	GROUP BY T.AlbumId
);
-- Casi, pero no!

-- Resultado correcto
-- Resultado final: La consulta principal es igual, pero añadiendo el HAVING para filtrar solo que la suma sea mayor que el de la subconsulta
-- que se encarga de calcular la duración media 
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING SUM(T.Milliseconds) > (
    SELECT AVG(DuracionTotal) 
    FROM (
        SELECT SUM(Milliseconds) AS DuracionTotal
        FROM Track
        GROUP BY AlbumId
    ) AS Suma_Canciones
);