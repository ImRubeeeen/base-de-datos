-- Mostrar las listas de reproducción en las que hay canciones de reggae.

-- Obtengo el ID del género reggae
SELECT GenreId
FROM Genre
WHERE Name = "reggae";

-- Obtengo los IDs de las canciones de este género
SELECT TrackId
FROM Track
WHERE GenreId IN (
	SELECT GenreId
	FROM Genre
	WHERE Name = "reggae"	
);

-- Obtener las listas de reproducción en las que hay canciones de reggae
SELECT DISTINCT P.PlaylistId, P.Name
FROM Playlist P
JOIN PlaylistTrack PT ON P.PlaylistId = PT.PlaylistId
WHERE PT.TrackId IN (
    SELECT T.TrackId
    FROM Track T
    WHERE T.GenreId = (
        SELECT G.GenreId
        FROM Genre G
        WHERE G.Name = 'Reggae'
    )
);