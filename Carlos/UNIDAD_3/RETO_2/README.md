# 🔐 RETO 3.2 DCL - Control de acceso 🔐

## 1. 🧑‍💻 Registrar, Modificar y Eliminar Usuarios

### 1.1. 📝 Registrar Nuevos Usuarios
Para registrar nuevos usuarios al sistema hay que ejecutar la siguiente orden:
```sql
CREATE USER 'nuevo_usuario'@'localhost' IDENTIFIED BY 'contraseña';
```
* *Nuevo usuario*: Tendremos que sustituir las comillas 'nuevo_usuario' por el nombre que le queramos dar, siempre entre las comillas.
* *Contraseña*: Para añadirle contraseña al usuario tendremos que sustituir las comillas de 'contraseña' por la contraseña que queramos otorgarle a ese usuario, entre las comillas.

### 1.2. 🔄 Modificar Usuarios

#### 1.2.1. 🛡️ Otorgar Todos los Privilegios
Para otorgar todos los privilegios dentro del sistema al usuario habrá que ejecutar lo siguiente:
```sql
GRANT ALL PRIVILEGES ON * . * TO 'nuevo_usuario'@'localhost';
```
#### 1.2.2. 🚫 Quitar Todos los Permisos
Con esta sentencia quitamos todos los privilegios que tenga otorgados el usuario que queramos.
```sql
REVOKE ALL PRIVILEGES ON *.* FROM 'usuario'@'localhost';
```
#### 1.2.3. 🔍 Consultar Usuarios
Mediante esta consulta veremos los usuarios existentes.
```sql
SELECT User, Host FROM mysql.user;
```
#### 1.2.4. 🗃️ Consultar Permisos de los Usuarios
Con esta consulta podremos ver que permisos tiene otorgado un usuario en concreto.
```sql
SHOW GRANTS FOR 'usuario'@'localhost';
```
#### 1.2.5. 🔑 Cambiar Contraseña del Usuario
Con este comando de terminal podremos cambiar la contraseña de un usuario en especifico.
```powershell
mysql -u root -p -e "ALTER USER 'usuario'@'localhost' IDENTIFIED BY 'nueva_contraseña'"
```

## 2. 2. 🔐 Autenticación de Usuarios y Opciones Disponibles en el SGBD
### 2.1. 📋 Tipos de Autenticación de MySQL
#### 2.1.1. 🔐 Autenticación Nativa
Se usa el usuario y la contraseña guardadas en las tablas internas de MySQL para acceder.
#### 2.1.2. 🖥️ Autenticación vía MySQL Workbench (SGBD)
##### 2.1.2.1. 🔐 Acceso vía autentificación Nativa
Al igual que de forma nativa, tendrás que poner tu usuario y contraseña pero además tendrás que poner la dirección IP dónde esta alojada la base de datos y el puerto expuesto.
##### 2.1.2.2. 🔒 Acceso vía SSL
Esta forma permite conectarte a la base de datos de manera cifrada, recomendado si te conectas vía internet a la BBDD.

### 3. 🛠️ Permisos que Puede Tener un Usuario y Granularidad
#### 3.1. 🛡️ Otorgar Permisos en Concreto
MySQL es bastante flexible en cuanto a otorgar permisos, podemos limitar la operavilidad de un usuario todo lo que queramos, por ejemplo, si queremos limitar al usuario que haga DELECT y SELECT tendremos que poner:
```sql
GRANT SELECT, DELETE
    ON nombredelabasededatos.* --el * hace que se aplique en todas las tablas,  si quisieramos solo en una tabla habría que especificar
    TO 'nombre_usuario'@'localhost';
```
Finalmente, deberemos ejecutar lo siguiente:
```sql
FLUSH PRIVILEGES;
```
#### 3.2. 🚫 Quitar Privilegios en Concreto
Mediante el siguiente comando podremos permisos en concreto.
```sql
REVOKE UPDATE, DELETE ON *.* FROM 'usuario'@'localhost';
```
En este caso, he eliminado el UPDATE y DELETE del usuario en todas las BBDD