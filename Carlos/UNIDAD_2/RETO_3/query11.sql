-- Carlos C de DAW1 ^^
-- Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

SELECT
    e.FirstName,
    e.LastName,
    (
        -- Subconsulta correlacionada para contar el número de clientes asignados a cada empleado
        SELECT COUNT(*)
        FROM Customer AS c
        WHERE c.SupportRepId = e.EmployeeId
    ) AS NumeroClientes
FROM
    Employee AS e
WHERE
    e.Title = 'Sales Support Agent'; -- Filtro opcional para obtener solo los empleados que son agentes de soporte de ventas
