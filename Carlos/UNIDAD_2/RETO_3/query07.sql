-- Carlos C de DAW1 ^^
-- Obtener los álbumes con una duración total superior a la media.
SELECT 
    A.Title AS AlbumTitulo, -- Selecciona el título del álbum
    AR.Name AS ArtistaNombre, -- Selecciona el nombre del artista
    Durations.TotalDuration AS DuracionTotal -- Selecciona la duración total del álbum
FROM 
    Album AS A -- Desde la tabla Album
JOIN 
    Artist AS AR ON A.ArtistId = AR.ArtistId -- Une con la tabla Artist usando ArtistId
JOIN 
    (
        SELECT 
            T.AlbumId, -- Selecciona el AlbumId
            SUM(T.Milliseconds) AS TotalDuration -- Suma la duración de las canciones por álbum
        FROM 
            Track AS T -- Desde la tabla Track
        GROUP BY 
            T.AlbumId -- Agrupa por AlbumId
    ) AS Durations ON A.AlbumId = Durations.AlbumId -- Une la subconsulta con la tabla Album usando AlbumId
WHERE 
    Durations.TotalDuration > (
        SELECT 
            AVG(TotalDurations.AvgDuration) -- Calcula la duración media de los álbumes
        FROM 
            (
                SELECT 
                    SUM(T.Milliseconds) AS AvgDuration -- Suma la duración de las canciones por álbum
                FROM 
                    Track AS T -- Desde la tabla Track
                GROUP BY 
                    T.AlbumId -- Agrupa por AlbumId
            ) AS TotalDurations
    );
