-- Carlos C de DAW1 ^^
-- Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

SELECT 
    InvoiceId, -- Selecciona el ID de la factura
    InvoiceDate, -- Selecciona la fecha de la factura
    Total -- Selecciona el total de la factura
FROM 
    Invoice -- Selecciona de la tabla "Invoice"
WHERE 
    CustomerId = (
        SELECT 
            CustomerId -- Selecciona el ID del cliente
        FROM 
            Customer -- Selecciona de la tabla "Customer"
        WHERE 
            Email = 'emma_jones@hotmail.com' -- Filtra por el email del cliente
    )
ORDER BY 
    InvoiceDate DESC -- Ordena las facturas por fecha en orden descendente
LIMIT 
    5; -- Limita los resultados a las 5 primeras facturas
