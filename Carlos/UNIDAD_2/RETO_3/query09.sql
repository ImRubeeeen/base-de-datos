-- Carlos C de DAW1 ^^
-- Canciones de la _playlist_ con más canciones.
SELECT 
    Track.Name AS NombreCancion, -- Selecciona el nombre de la canción
    Playlist.Name AS NombrePlaylist -- Selecciona el nombre de la playlist
FROM 
    Playlist -- Desde la tabla Playlist
JOIN 
    PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId -- Une con la tabla PlaylistTrack usando PlaylistId
JOIN 
    Track ON PlaylistTrack.TrackId = Track.TrackId -- Une con la tabla Track usando TrackId
WHERE 
    Playlist.PlaylistId = (
        SELECT 
            Playlist.PlaylistId -- Selecciona el PlaylistId
        FROM 
            Playlist -- Desde la tabla Playlist
        JOIN 
            PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId -- Une con la tabla PlaylistTrack usando PlaylistId
        GROUP BY 
            Playlist.PlaylistId -- Agrupa por PlaylistId
        ORDER BY 
            COUNT(PlaylistTrack.TrackId) DESC -- Ordena por el número de canciones en orden descendente
        LIMIT 1 -- Selecciona solo el primer resultado
    );
