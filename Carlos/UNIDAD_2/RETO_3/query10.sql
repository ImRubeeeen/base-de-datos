-- Carlos C de DAW1 ^^
-- Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

SELECT
    c.FirstName, -- Selecciona el nombre del cliente
    c.LastName, -- Selecciona el apellido del cliente
    c.CustomerId, -- Selecciona el ID del cliente
    (
        -- Subconsulta correlacionada para calcular la suma total de las compras de cada cliente
        SELECT SUM(i.Total)
        FROM Invoice AS i
        WHERE i.CustomerId = c.CustomerId
    ) AS TotalGastado -- Alias para la suma total de compras
FROM
    Customer AS c; -- Desde la tabla Customer
