-- Carlos C de DAW1 ^^
-- Canciones del género con más canciones.

-- Obtener las canciones del género con más canciones
SELECT 
    T.Name AS NombreCancion, -- Selecciona el nombre de la canción
    G.Name AS NombreGenero -- Selecciona el nombre del género
FROM 
    Track AS T -- Desde la tabla Track
JOIN 
    Genre AS G ON T.GenreId = G.GenreId -- Une con la tabla Genre usando GenreId
WHERE 
    T.GenreId = (
        SELECT 
            GenreId -- Selecciona el GenreId
        FROM 
            Track
        GROUP BY 
            GenreId -- Agrupa por GenreId
        ORDER BY 
            COUNT(TrackId) DESC -- Ordena por el número de canciones en orden descendente
        LIMIT 1 -- Selecciona solo el primer resultado
    );
