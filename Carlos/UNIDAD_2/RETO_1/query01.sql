-- Carlos C de DAW1 ^^
-- Encuentra todos los clientes de Francia.

-- selecciono todo de la base de datos Chinook
SELECT *
-- dentro de ella me posiciono en Customer (clientes)
FROM Chinook.Customer
-- filtro con el "WHERE" los clientes de Francia
WHERE country = "France";
