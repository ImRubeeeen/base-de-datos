-- Carlos C de DAW1 ^^
-- Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las 
-- columnas: fecha de la factura, nombre completo del cliente, dirección de facturación,
-- código postal, país, importe (en este orden).

SELECT 
    Invoice.InvoiceDate AS FechaFactura, -- Seleccionamos la fecha de la factura y la aliasamos como "FechaFactura"
    CONCAT(Customer.FirstName, ' ', Customer.LastName) AS NombreCompleto, -- Concatenamos el nombre y el apellido del cliente y lo aliasamos como "NombreCompleto"
    Invoice.BillingAddress AS DireccionFacturacion, -- Seleccionamos la dirección de facturación y la aliasamos como "DireccionFacturacion"
    Invoice.BillingPostalCode AS CodigoPostal, -- Seleccionamos el código postal y lo aliasamos como "CodigoPostal"
    Invoice.BillingCountry AS Pais, -- Seleccionamos el país y lo aliasamos como "Pais"
    Invoice.Total AS Importe -- Seleccionamos el importe total y lo aliasamos como "Importe"
FROM 
    Invoice -- Desde la tabla Invoice
JOIN 
    Customer ON Invoice.CustomerId = Customer.CustomerId -- Unimos con la tabla Customer usando CustomerId para obtener información del cliente
WHERE 
    Customer.City = 'Berlin' -- Filtramos por clientes que residan en Berlín
ORDER BY 
    Invoice.InvoiceDate; -- Ordenamos los resultados por fecha de la factura
