-- Carlos C de DAW1 ^^
-- Muestra el número de canciones del álbum “Out Of Time”.

SELECT 
    COUNT(*) AS NumeroCanciones -- Contamos todas las filas resultantes y lo mostramos como "NumeroCanciones"
FROM 
    Album -- Desde la tabla Album
JOIN 
    Track ON Album.AlbumId = Track.AlbumId -- Unimos con la tabla Track usando AlbumId para obtener información de las canciones asociadas al álbum
WHERE 
    Album.Title = 'Out Of Time'; -- Filtramos por el título del álbum "Out Of Time"