-- Carlos C de DAW1 ^^
-- Muestra el número total de artistas.

SELECT 
    COUNT(*) AS NumeroTotalArtistas -- Contamos todas las filas de la tabla Artist y lo mostramos como "NumeroTotalArtistas"
FROM 
    Artist; -- Desde la tabla Artist
