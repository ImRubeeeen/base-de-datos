-- Carlos C de DAW1 ^^
-- Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas
-- sus canciones, ordenadas por álbum y por duración.
SELECT 
    Playlist.Name AS NombreListaReproduccion, -- Seleccionamos el nombre de la lista de reproducción y lo aliasamos como "NombreListaReproduccion"
    Track.Name AS NombreCancion, -- Seleccionamos el nombre de la canción y lo aliasamos como "NombreCancion"
    Album.Title AS TituloAlbum, -- Seleccionamos el título del álbum y lo aliasamos como "TituloAlbum"
    Track.Milliseconds AS Duracion -- Seleccionamos la duración de la canción (en milisegundos) y lo aliasamos como "Duracion"
FROM 
    Playlist -- Desde la tabla Playlist
JOIN 
    PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId -- Unimos con la tabla PlaylistTrack usando PlaylistId para obtener las relaciones entre listas de reproducción y canciones
JOIN 
    Track ON PlaylistTrack.TrackId = Track.TrackId -- Unimos con la tabla Track usando TrackId para obtener información de las canciones
JOIN 
    Album ON Track.AlbumId = Album.AlbumId -- Unimos con la tabla Album usando AlbumId para obtener información de los álbumes
WHERE 
    Playlist.Name LIKE 'C%' -- Filtramos por listas de reproducción cuyo nombre comienza con 'C'
ORDER BY 
    Playlist.Name, -- Ordenamos primero por nombre de lista de reproducción
    Album.Title, -- Luego por título de álbum
    Track.Milliseconds; -- Finalmente por duración de la canción
