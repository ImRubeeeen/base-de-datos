-- Carlos C de DAW1 ^^
-- Muestra los países en los que tenemos al menos 5 clientes.

SELECT 
    Country, -- Seleccionamos el nombre del país
    COUNT(CustomerId) AS TotalClientes -- Contamos el número de clientes en cada país y lo mostramos como "TotalClientes"
FROM 
    Customer -- Desde la tabla Customer
GROUP BY 
    Country -- Agrupamos los resultados por país
HAVING 
    COUNT(CustomerId) >= 5; -- Filtramos los resultados para mostrar solo países con al menos 5 clientes
