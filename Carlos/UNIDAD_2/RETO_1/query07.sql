-- Carlos C de DAW1 ^^
-- Muestra todos los artistas junto a sus álbumes.
SELECT
    Artist.Name AS ArtistName, -- Seleccionamos el nombre del artista y lo aliasamos como ArtistName
    Album.Title AS AlbumTitle -- Seleccionamos el título del álbum y lo aliasamos como AlbumTitle
FROM 
    Artist -- Desde la tabla Artist
JOIN 
    Album ON Artist.ArtistId = Album.ArtistId -- Unimos con la tabla Album usando ArtistId para relacionar artistas con sus álbumes
ORDER BY 
    Artist.Name, Album.Title; -- Ordenamos por el nombre del artista y el título del álbum