-- Carlos C de DAW1 ^^
-- Muestra las facturas del primer trimestre de este año.

-- selecciono todo de la base de datos Chinook
SELECT *
-- dentro de ella me posiciono en Invoice (facturas)
FROM Chinook.Invoice
-- filtro con "WHERE" las facturas de ENERO hasta MARZO usando "BETWEEN"
WHERE InvoiceDate BETWEEN '2024-01-01' AND '2024-03-31';
