-- Carlos C de DAW1 ^^
-- Muestra todas las canciones compuestas por AC/DC.

-- selecciono todo de la base de datos Chinook
SELECT *
-- dentro de ella me posiciono en Track (cancion)
FROM Chinook.Track
-- filtro con el "WHERE" las canciones de AC/DC
WHERE Composer = "AC/DC";
