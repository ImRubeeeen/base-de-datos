-- Carlos C de DAW1 ^^
-- Muestra todas las canciones que no han sido compradas.
SELECT 
    t.TrackId, -- Seleccionamos el ID de la canción
    t.Name AS TrackName, -- Seleccionamos el nombre de la canción
    t.AlbumId, -- Seleccionamos el ID del álbum al que pertenece la canción
    t.MediaTypeId, -- Seleccionamos el ID del tipo de medio de la canción
    t.GenreId, -- Seleccionamos el ID del género de la canción
    t.Composer, -- Seleccionamos el compositor de la canción
    t.Milliseconds, -- Seleccionamos la duración de la canción en milisegundos
    t.Bytes, -- Seleccionamos el tamaño de la canción en bytes
    t.UnitPrice -- Seleccionamos el precio unitario de la canción
FROM 
    Track t -- Desde la tabla Track, la aliasamos como "t"
WHERE 
    NOT EXISTS (
        -- Subconsulta: Verificamos si no existe ninguna línea de factura que contenga esta canción
        SELECT 
            1 -- No importa qué columna seleccionamos, solo necesitamos verificar la existencia de al menos una fila
        FROM 
            InvoiceLine il -- Desde la tabla InvoiceLine, la aliasamos como "il"
        WHERE 
            il.TrackId = t.TrackId -- Comparamos el ID de la canción en la línea de factura con el ID de la canción en la tabla Track
    );
