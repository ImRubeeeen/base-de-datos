-- Carlos C de DAW1 ^^
-- Clientes que han comprado todos los álbumes de un artista específico.

SELECT 
    c.CustomerId, -- Seleccionamos el ID del cliente
    c.FirstName, -- Seleccionamos el primer nombre del cliente
    c.LastName -- Seleccionamos el apellido del cliente
FROM 
    Customer c -- Desde la tabla Customer, la aliasamos como "c"
WHERE 
    (
        -- Subconsulta para contar el número total de álbumes del artista específico
        SELECT 
            COUNT(DISTINCT a.AlbumId) -- Contamos el número de álbumes únicos del artista específico
        FROM 
            Album a -- Desde la tabla Album, la aliasamos como "a"
            JOIN Artist ar ON a.ArtistId = ar.ArtistId -- Unimos Album con Artist usando ArtistId para obtener el nombre del artista
        WHERE 
            ar.Name = 'Nombre del Artista' -- Filtramos por el nombre del artista específico
    ) = (
        -- Subconsulta para contar el número de álbumes comprados por el cliente para el artista específico
        SELECT 
            COUNT(DISTINCT al.AlbumId) -- Contamos el número de álbumes únicos comprados por el cliente para el artista específico
        FROM 
            Album al -- Desde la tabla Album, la aliasamos como "al"
            JOIN Artist art ON al.ArtistId = art.ArtistId -- Unimos Album con Artist usando ArtistId para obtener el nombre del artista
            JOIN Track t ON al.AlbumId = t.AlbumId -- Unimos Album con Track usando AlbumId para obtener las canciones
            JOIN InvoiceLine il ON t.TrackId = il.TrackId -- Unimos Track con InvoiceLine usando TrackId para obtener las líneas de factura
            JOIN Invoice i ON il.InvoiceId = i.InvoiceId -- Unimos InvoiceLine con Invoice usando InvoiceId para obtener las facturas
        WHERE 
            art.Name = 'Nombre del Artista' -- Filtramos por el nombre del artista específico
            AND i.CustomerId = c.CustomerId -- Comparamos el ID del cliente con el ID del cliente en la factura
    );
