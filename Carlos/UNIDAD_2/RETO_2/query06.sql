-- Carlos C de DAW1 ^^
-- Muestra todas las canciones que ha comprado el cliente Luis Rojas.

SELECT 
    t.TrackId, -- Seleccionamos el ID de la canción
    t.Name AS TrackName, -- Seleccionamos el nombre de la canción
    t.AlbumId, -- Seleccionamos el ID del álbum al que pertenece la canción
    t.MediaTypeId, -- Seleccionamos el ID del tipo de medio de la canción
    t.GenreId, -- Seleccionamos el ID del género de la canción
    t.Composer, -- Seleccionamos el compositor de la canción
    t.Milliseconds, -- Seleccionamos la duración de la canción en milisegundos
    t.Bytes, -- Seleccionamos el tamaño de la canción en bytes
    t.UnitPrice -- Seleccionamos el precio unitario de la canción
FROM 
    Track t -- Desde la tabla Track, la aliasamos como "t"
WHERE 
    t.TrackId IN ( -- Filtramos las canciones que están incluidas en la subconsulta
        SELECT 
            il.TrackId -- Seleccionamos los IDs de las canciones de las líneas de factura
        FROM 
            InvoiceLine il -- Desde la tabla InvoiceLine, la aliasamos como "il"
            -- Unimos InvoiceLine con Invoice usando InvoiceId para obtener información de las facturas
            JOIN Invoice i ON il.InvoiceId = i.InvoiceId
            -- Unimos Invoice con Customer usando CustomerId para obtener información del cliente
            JOIN Customer c ON i.CustomerId = c.CustomerId
        WHERE 
            c.FirstName = 'Luis' AND c.LastName = 'Rojas' -- Filtramos las facturas asociadas al cliente "Luis Rojas"
    );