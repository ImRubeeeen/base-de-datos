-- Carlos C de DAW1 ^^
-- Muestra todos los artistas que tienen canciones con duración superior a 5 minutos.

SELECT DISTINCT
    ar.Name AS ArtistName -- Seleccionamos el nombre del artista y lo aliasamos como "ArtistName"
FROM
    Artist ar
    -- Join para relacionar Artist con Album
    JOIN Album al ON ar.ArtistId = al.ArtistId
    -- Join para relacionar Album con Track
    JOIN Track t ON al.AlbumId = t.AlbumId
WHERE
    t.Milliseconds > 300000; -- Filtro las canciones con duración superior a 5 minutos (300000 milisegundos)
