-- Carlos C de DAW1 ^^
-- Canciones que son más caras que cualquier otra canción del mismo álbum.

SELECT 
    t.TrackId, -- Seleccionamos el ID de la canción
    t.Name AS TrackName, -- Seleccionamos el nombre de la canción
    t.UnitPrice -- Seleccionamos el precio unitario de la canción
FROM 
    Track t -- Desde la tabla Track, la aliasamos como "t"
WHERE 
    t.UnitPrice > ALL ( -- Filtramos las canciones cuyo precio unitario es mayor que todos los precios unitarios de las demás canciones del mismo álbum
        SELECT 
            t2.UnitPrice -- Seleccionamos los precios unitarios de todas las canciones del mismo álbum
        FROM 
            Track t2 -- Desde la tabla Track, la aliasamos como "t2"
        WHERE 
            t2.AlbumId = t.AlbumId -- Filtro por el mismo álbum
            AND t2.TrackId <> t.TrackId -- Excluyo la canción actual
    );