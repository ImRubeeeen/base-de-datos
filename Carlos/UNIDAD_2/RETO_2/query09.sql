-- Carlos C de DAW1 ^^
-- Clientes que han comprado más de 20 canciones en una sola transacción.

SELECT 
    C.FirstName AS Nombre, -- Selecciona el primer nombre del cliente y lo nombra como "Nombre"
    C.LastName AS Apellido, -- Selecciona el apellido del cliente y lo nombra como "Apellido"
    COUNT(IL.TrackId) AS CancionesCompradas -- Cuenta cuántas canciones ha comprado el cliente y lo nombra como "CancionesCompradas"
FROM 
    Customer AS C -- Alias de la tabla "Customer" como "C"
JOIN 
    Invoice AS I ON C.CustomerId = I.CustomerId -- Une la tabla "Customer" con la tabla "Invoice" utilizando el CustomerId
JOIN 
    InvoiceLine AS IL ON I.InvoiceId = IL.InvoiceId -- Une la tabla "Invoice" con la tabla "InvoiceLine" utilizando el InvoiceId
GROUP BY 
    C.CustomerId, C.FirstName, C.LastName -- Agrupa los resultados por CustomerId, FirstName y LastName del cliente
HAVING 
    COUNT(IL.TrackId) > 20; -- Filtra los resultados para incluir solo aquellos clientes que han comprado más de 20 canciones en una sola transacción
