-- Carlos C de DAW1 ^^
-- Para demostrar lo bueno que es nuestro servicio, muestra el número de países
-- donde tenemos clientes, el número de géneros músicales de los que disponemos y
-- el número de pistas.

SELECT 
    (SELECT COUNT(DISTINCT Country) FROM Customer) AS NumeroPaises,

-- Número de géneros musicales disponibles
    (SELECT COUNT(*) FROM Genre) AS NumeroGenerosMusicales,

-- Número de pistas en nuestra colección
    (SELECT COUNT(*) FROM Track) AS NumeroPistas;
