-- Carlos C de DAW1 ^^
-- Etiqueta a los clientes como “VIP” si han gastado más de $500 en compras totales.

SELECT 
    CustomerId, -- Selecciona el ID del cliente
    FirstName, -- Selecciona el primer nombre del cliente
    LastName, -- Selecciona el apellido del cliente
    (
        SELECT 
            SUM(Total) -- Utiliza una subconsulta para obtener el total gastado por el cliente
        FROM 
            Invoice -- Selecciona de la tabla "Invoice"
        WHERE 
            Customer.CustomerId = Invoice.CustomerId -- Filtra las facturas por el ID del cliente actual
    ) AS TotalGastado, -- El resultado de la suma total gastada se nombra como "TotalGastado"
    CASE -- Utiliza una expresión CASE para etiquetar al cliente como "VIP" si han gastado más de $500
        WHEN (
            SELECT 
                SUM(Total) -- Utiliza una subconsulta para obtener el total gastado por el cliente
            FROM 
                Invoice -- Selecciona de la tabla "Invoice"
            WHERE 
                Customer.CustomerId = Invoice.CustomerId -- Filtra las facturas por el ID del cliente actual
        ) > 500 THEN 'VIP' -- Etiqueta como "VIP" si han gastado más de $500
        ELSE 'No VIP' -- Etiqueta como "No VIP" si han gastado $500 o menos
    END AS Etiqueta -- El resultado de la etiqueta se nombra como "Etiqueta"
FROM 
    Customer; -- Selecciona de la tabla "Customer"
