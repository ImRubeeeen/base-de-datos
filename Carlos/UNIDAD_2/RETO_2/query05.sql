-- Carlos C de DAW1 ^^
-- Lista los empleados junto a sus subordinados (empleados que reportan a ellos).

SELECT 
    e.EmployeeId AS EmployeeID, -- Seleccionamos el ID del empleado y lo aliasamos como "EmployeeID"
    e.FirstName AS EmployeeFirstName, -- Seleccionamos el primer nombre del empleado y lo aliasamos como "EmployeeFirstName"
    e.LastName AS EmployeeLastName, -- Seleccionamos el apellido del empleado y lo aliasamos como "EmployeeLastName"
    e.Title AS EmployeeTitle, -- Seleccionamos el título del empleado y lo aliasamos como "EmployeeTitle"
    (
        -- Subconsulta: Contamos el número de subordinados directos de cada empleado
        SELECT 
            COUNT(*) -- Contamos todas las filas de la tabla Employee que tengan el campo ReportsTo igual al ID del empleado actual
        FROM 
            Employee sub -- Desde la tabla Employee, la aliasamos como "sub"
        WHERE 
            sub.ReportsTo = e.EmployeeId -- Filtramos las filas donde el campo ReportsTo sea igual al ID del empleado actual
    ) AS NumberOfSubordinates -- Mostramos el número de subordinados como "NumberOfSubordinates"
FROM 
    Employee e; -- Desde la tabla Employee, la aliasamos como "e"
