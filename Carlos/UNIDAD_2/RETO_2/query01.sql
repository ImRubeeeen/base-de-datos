-- Carlos C de DAW1 ^^
-- Muestra las listas de reproducción cuyo nombre comienza por M, junto a las 3
-- primeras canciones de cada uno, ordenadas por álbum y por precio (más bajo primero).

SELECT 
    Playlist.Name AS NombreListaReproduccion, -- Seleccionamos el nombre de la lista de reproducción y lo aliasamos como "NombreListaReproduccion"
    Track.Name AS NombreCancion, -- Seleccionamos el nombre de la canción y lo aliasamos como "NombreCancion"
    Album.Title AS TituloAlbum, -- Seleccionamos el título del álbum y lo aliasamos como "TituloAlbum"
    Track.UnitPrice AS Precio -- Seleccionamos el precio de la canción y lo aliasamos como "Precio"
FROM 
    Playlist -- Desde la tabla Playlist
JOIN 
    PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId -- Unimos con la tabla PlaylistTrack usando PlaylistId para obtener las relaciones entre listas de reproducción y canciones
JOIN 
    (
        -- Subconsulta para numerar las canciones por álbum y ordenarlas por precio de canción
        SELECT 
            TrackId,
            AlbumId,
            Name,
            UnitPrice,
            ROW_NUMBER() OVER (PARTITION BY AlbumId ORDER BY UnitPrice) AS row_num -- Numeramos las canciones por álbum y ordenamos por precio de canción
        FROM 
            Track
    ) AS TrackWithRowNumber ON PlaylistTrack.TrackId = TrackWithRowNumber.TrackId -- Unimos con la subconsulta usando TrackId para obtener las canciones numeradas
JOIN 
    Track ON PlaylistTrack.TrackId = Track.TrackId -- Unimos con la tabla Track para obtener información adicional de las canciones
JOIN 
    Album ON TrackWithRowNumber.AlbumId = Album.AlbumId -- Unimos con la tabla Album usando AlbumId para obtener información de los álbumes
WHERE 
    Playlist.Name LIKE 'M%' -- Filtramos por listas de reproducción cuyo nombre comienza con 'M'
    AND TrackWithRowNumber.row_num <= 3 -- Filtramos solo las tres primeras canciones de cada lista de reproducción
ORDER BY 
    Album.Title, -- Ordenamos por título del álbum
    Track.UnitPrice; -- Luego por precio de la canción