-- Carlos C de DAW1 ^^
-- Clasifica a los clientes en tres categorías según su país: “Local” si el país es
-- ‘USA’, “Internacional” si el país es distinto de ‘USA’, y “Desconocido” si el país
-- es nulo.

SELECT 
    FirstName, -- Selecciona el primer nombre del cliente
    LastName, -- Selecciona el apellido del cliente
    Country, -- Selecciona el país del cliente
    CASE -- Utiliza una expresión CASE para clasificar a los clientes según su país
        WHEN Country IS NULL THEN 'Desconocido' -- Si el país es nulo, clasificar como "Desconocido"
        WHEN Country = 'USA' THEN 'Local' -- Si el país es 'USA', clasificar como "Local"
        ELSE 'Internacional' -- Si el país es distinto de 'USA', clasificar como "Internacional"
    END AS Categoria -- El resultado de la clasificación se nombra como "Categoria"
FROM 
    (SELECT 
        FirstName, -- Selecciona el primer nombre del cliente
        LastName, -- Selecciona el apellido del cliente
        Country -- Selecciona el país del cliente
    FROM 
        Customer) AS CustomerData; -- Utiliza una subconsulta para obtener los datos de los clientes y la aliasa como "CustomerData"
