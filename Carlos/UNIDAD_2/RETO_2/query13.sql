-- Carlos C de DAW1 ^^
-- Canciones vendidas más que la media de ventas por canción.

SELECT 
    T.Name AS NombreCancion -- Selecciona el nombre de la canción
FROM 
    Track AS T -- Selecciona de la tabla "Track"
JOIN 
    InvoiceLine AS IL ON T.TrackId = IL.TrackId -- Une la tabla "Track" con la tabla "InvoiceLine" utilizando el TrackId
GROUP BY 
    T.Name -- Agrupa los resultados por el nombre de la canción
HAVING 
    COUNT(IL.InvoiceLineId) > ( -- Compara la cantidad de ventas con la media de ventas por canción
        SELECT 
            AVG(Subquery.NumVentas) -- Calculamos la media de ventas por canción utilizando una subconsulta
        FROM 
            (SELECT 
                 TrackId,
                 COUNT(*) AS NumVentas -- Contamos el número de ventas por canción
             FROM 
                 InvoiceLine -- Desde la tabla "InvoiceLine"
             GROUP BY 
                 TrackId) AS Subquery -- Agrupamos por el TrackId y lo aliasamos como "Subquery"
    );
