# Reto 3.1: Definición de datos en MySQL
**[Bases de Datos] Unidad Didáctica 3: DLL - APUNTES**

**Rubén Martí Garcia**

En este reto, vamos a aprender a como conectarnos a MySQL CLI (línea de comandos), qué es el TCL y como crear
una BBDD, tablas y cómo modificar un campo ya creado.

## 1. Conexión con MySQL CLI:

### ¿Qué es el CLI de MySQL?

Es el **Command Line Client** que tiene. De la siguiente forma nos conectamos:

```sql
mysql -u root -p -h 127.0.0.1 -P 33006
```

### Ver la base de datos:

```sql
SHOW DATABASES;
```

**Hay otro comando con el que se puede ver las bases de datos:**

```sql
SHOW SCHEMAS;
```

### Como seleccionar una base de datos?

```sql
USE Chinook;
```

### Ver tablas:

```sql
SHOW TABLES;
```

### Ver tablas de forma completa:

```sql
SHOW FULL TABLES;
```

### Ver lo que tiene una tabla:

```sql
DESCRIBE Employee;
```

### Ver las columnas de una tabla:

```sql
SHOW COLUMNS FROM Employee;
```

## 2. TCL: COMMIT, ROLLBACK y Autocommit

### ¿Qué es el TCL y para qué sirve?

El el **Lenguaje de Control Transaccional** y sirve para administrar diferentes transacciones que ocurren dentro de una base de datos.

### ¿Qué posibilidades tenemos?

- Commit
- Autocommit
- Rollback

#### Commit:

Se utiliza para cuando hago cambios y quiero que se apliquen en la BBDD. Se utiliza lo siguiente:

```sql
COMMIT;
```

##### Ver si tienes el autocommit seleccionado:

El autocommit es un commit que puede venir por defecto activado y no hace falta hacer commits todo el rato. Para poder verlo si lo tienes activado tienes que poner lo siguiente: 

```sql
SELECT @@autocommit;
```

##### Modificar el autocommit:

De la siguiente forma pongo que no se haga commits de forma automática.

```sql
SET autocommit=0;
```

#### Rollback:

Se usa para volver hacía atrás cuando te has equivocado modificando o añadiendo algun dato nuevo y te volverá al __ÚLTIMO__ commit hecho.

```sql
ROLLBACK;
```

## 3. Creación de BBDD, tablas y modificación de campos:

### Crear BBDD:

```sql
CREATE SCHEMA `Rallané` ;
```

También se puede usar **DATABSE**:

```sql
CREATE DATABASE `Rallané` ;
```
### Crear tablas y añadir campos:

```sql
CREATE TABLE `Rallané`.`Pasajeros` (
  `idPasajeros` INT NOT NULL,
  `Nombre` VARCHAR(100) NOT NULL,
  `Documento` VARCHAR(45) NOT NULL,
  `TipoDocumento` VARCHAR(15) NOT NULL,
  `FechaNacimiento` DATE NOT NULL,
  `Nacionalidad` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPasajeros`),
  UNIQUE INDEX `Documento_UNIQUE` (`Documento` ASC) VISIBLE);
```

```sql
CREATE TABLE `Rallané`.`Vuelos` (
  `idVuelo` INT NOT NULL,
  `NPlazas` INT NULL,
  `Origen` VARCHAR(50) NULL,
  `Destino` VARCHAR(50) NULL,
  `Compañía` VARCHAR(45) NULL,
  `FechaHora` DATETIME NULL,
  PRIMARY KEY (`idVuelo`));
```

Habría que hacer una tercera tabla. Ésta se hace para poder relacionar los vuelos con los pasajeros, ya que es una relación de muchos a muchos (N:M).

```sql
CREATE TABLE `Rallané`.`Reservas` (
  `id_Reservas` int NOT NULL,
  `id_vuelo` int NOT NULL,
  `id_pasajero` int NOT NULL,
  `n_asiento` varchar(11) NOT NULL,
  PRIMARY KEY (`id_Reservas`,`id_vuelo`,`id_pasajero`),
  KEY `id_Reservas` (`id_Reservas`)
);
```

### Modificar columnas:

```sql
ALTER TABLE `Rallané`.`Pasajeros` 
CHANGE COLUMN `idPasajeros` `idPasajero` INT NOT NULL ;
```

```sql
ALTER TABLE `Rallané`.`Reservas` 
CHANGE COLUMN `idPasajero` `idPasajero` INT NOT NULL ,
CHANGE COLUMN `idVuelo` `idVuelo` INT NOT NULL ;
```

### Hacer Foreign Key:

Se utilizan para garantizar la integridad entre dos tablas, asegurando que los valores en una columna de una tabla coincidan con los valores en otra tabla.

```sql
ALTER TABLE `Rallané`.`Reservas` 
ADD INDEX `fk_Reservas_1_idx` (`id_pasajero` ASC) VISIBLE;
;
ALTER TABLE `Rallané`.`Reservas` 
ADD CONSTRAINT `fk_Reservas_1`
  FOREIGN KEY (`id_pasajero`)
  REFERENCES `Rallané`.`Pasajeros` (`idPasajeros`);
```

```sql
ALTER TABLE `Rallané`.`Reservas` 
ADD INDEX `fk_Reservas_2_idx` (`id_vuelo` ASC) VISIBLE;
;
ALTER TABLE `Rallané`.`Reservas` 
ADD CONSTRAINT `fk_Reservas_2`
  FOREIGN KEY (`id_vuelo`)
  REFERENCES `Rallané`.`Vuelos` (`idVuelo`);
```

## La sintaxis de la definición de los atributos de una tabla

En MySQL, la sintaxis para definir los atributos (o columnas) de una tabla sigue un orden específico. Aquí está el orden general y una descripción de cada parte:

- Nombre de la columna: El nombre de la columna.
- Tipo de dato: El tipo de dato de la columna (por ejemplo, INT, VARCHAR, DATE, etc.).
- Modificadores de tipo de dato: Modificadores como UNSIGNED, ZEROFILL, etc.
- Restricciones: Restricciones que se aplican a la columna (por ejemplo, NOT NULL, UNIQUE, PRIMARY KEY, AUTO_INCREMENT, etc.).
- Valor predeterminado: El valor predeterminado para la columna (DEFAULT).
- Comentarios: Comentarios sobre la columna (COMMENT).

### Ejemplo

```sql
CREATE TABLE ejemplo (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    fecha_nacimiento DATE,
    correo VARCHAR(255) UNIQUE,
    activo BOOLEAN DEFAULT TRUE,
    comentarios TEXT COMMENT 'Comentarios adicionales',
    PRIMARY KEY (id)
);
```

### Desglose

Desglosando cada columna de la tabla ejemplo:

1. id:

- Nombre de la columna: id
- Tipo de dato: INT
- Modificadores de tipo de dato: UNSIGNED
- Restricciones: NOT NULL, AUTO_INCREMENT
- Llave primaria: PRIMARY KEY

2. nombre:

- Nombre de la columna: nombre
- Tipo de dato: VARCHAR(100)
- Restricciones: NOT NULL

3. fecha_nacimiento:

- Nombre de la columna: fecha_nacimiento
- Tipo de dato: DATE

4. correo:

- Nombre de la columna: correo
- Tipo de dato: VARCHAR(255)
- Restricciones: UNIQUE

5. activo:

- Nombre de la columna: activo
- Tipo de dato: BOOLEAN
- Valor predeterminado: DEFAULT TRUE

6. comentarios:

- Nombre de la columna: comentarios
- Tipo de dato: TEXT
- Comentarios: COMMENT 'Comentarios adicionales'

Adicionalmente, se define una clave primaria (PRIMARY KEY) para la columna id.

## Campos generados

Son columnas cuyos valores se calculan automáticamente a partir de otras columnas en la misma tabla. Estas columnas pueden ser GENERATED ALWAYS AS (expresión) y pueden ser de dos tipos: almacenados (stored) o virtuales (virtual).

### Tipos de columnas generadas

1. Almacenadas (Stored):

- El valor se calcula y se almacena físicamente en la tabla.
- El cálculo se realiza cuando se inserta o actualiza una fila.
- Ocupa espacio en disco.
- Se puede indexar.

2. Virtuales (Virtual):

- El valor se calcula cada vez que se consulta la columna.
- No ocupa espacio en disco adicional.
- No se puede indexar directamente (aunque se pueden crear índices indirectos).

### Usos comunes

- Cálculos matemáticos: Para realizar operaciones aritméticas entre columnas.
- Normalización de datos: Para mantener consistencia de datos derivados de otros campos.
- Automatización: Para simplificar y automatizar procesos de negocio que requieren cálculos repetitivos.

### Ventajas y desventajas

1. Ventajas:

- Reducción de redundancia de datos.
- Actualización automática de los valores calculados.
- Simplificación de consultas complejas.

2. Desventajas:

- Las columnas almacenadas ocupan espacio adicional en disco.
- Las columnas virtuales no se pueden indexar directamente, lo que puede afectar el rendimiento de consultas complejas.

Las columnas generadas son una característica poderosa de MySQL que permite una mayor flexibilidad y eficiencia en la gestión y manipulación de datos.

### Ejemplos sintaxis

1. Columna generada almacenada:

```sql
CREATE TABLE ejemplo_almacenada (
    base DECIMAL(10,2),
    altura DECIMAL(10,2),
    area DECIMAL(10,2) GENERATED ALWAYS AS (base * altura) STORED
);

```

2. Columna generada virtual:

```sql
CREATE TABLE ejemplo_virtual (
    precio DECIMAL(10,2),
    cantidad INT,
    total DECIMAL(10,2) GENERATED ALWAYS AS (precio * cantidad) VIRTUAL
);
```