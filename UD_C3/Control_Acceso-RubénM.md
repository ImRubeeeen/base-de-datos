# Reto 3.2: Control de acceso de MySQL
**[Bases de Datos] Unidad Didáctica 3: DLL - APUNTES 2**

**Rubén Martí Garcia**

En este reto, vamos a aprender a como crear usuarios, como dar permisos y, cómo crear roles y asignarselos.

## 1. Ver usuarios

Aquí podemos ver los usuarios que tenemos en nuestra red.  

El % de la columna de "**host**" significa que puedes conectarte desde cualquier sitio, no solo en mi máquina (localhost).

```sql
SELECT user, host FROM mysql.user;
```

## 2. Crear usuarios, modificarlos y eliminarlos:

### Autenticación de Usuarios y Opciones Disponibles

MySQL permite diferentes métodos de autenticación:

#### Autenticación por contraseña estándar:

```sql
CREATE USER 'Alberto'@'localhost' IDENTIFIED BY '123';
```

#### Autenticación por plugin:

```sql
CREATE USER 'Alberto'@'%' REQUIRE SSL;
```

### Modificar usuario

```sql
RENAME USER 'Alberto'@'localhost' TO 'Alberto'@'%'
```

### Eliminar usuario

```sql
DROP USER 'prueba'@'localhost';
```

## 3. Ver permisos, darlos y borrarlos:

### Ver permisos

Con el siguiente comando veremos los permisos que tienes con el usuario en el que estás conectados.

```sql
SHOW GRANTS;
```

#### Ver permisos de una cuenta de un usuario

```sql
SHOW GRANTS FOR 'dbuser'@'%';
```

### Permisos y granudalidad

#### Granudalidad de los permisos

Niveles de granularidad:

1. Global
- Base de datos
- Tabla
- Columna
- Procedimientos/funciones
- Permisos comunes:

2. ALL PRIVILEGES
- SELECT, INSERT, UPDATE, DELETE
- CREATE, DROP, ALTER
- GRANT OPTION
- EXECUTE

#### Dar permisos

En este caso lo que hace es dale permiso de 'SELECT' a todas las tablas de la base de datos de "empresa" a Alberto, que solo puede conectarse desde localhost.

```sql
GRANT SELECT ON empresa.* TO 'Alberto'@'%';
```

Luego tenemos que aplicar la actualización de los privilegios:

```sql
FLUSH PRIVILEGES;
```

#### Borrar permisos

```sql
REVOKE ALL PRIVILEGES ON *.* FROM 'Alberto'@'%';
```

Luego tenemos que aplicar la actualización de los privilegios:

```sql
FLUSH PRIVILEGES;
```

#### Privilegios estaticos

Son privilegios que se pueden asignar directamente a los usuarios, como `por ejemplo: SELECT, INSERT, UPDATE, DELETE, CREATE, DROP...

#### Privilegios dinamicos

Son privilegios que pueden cambiar dependiendo del contexto o configuración del sistema, como los permisos otorgados por roles.

## 4. Roles

### Ver roles actuales

```sql
SELECT CURRENT_ROLE();
```

### Crear un rol

```sql
CREATE ROLE 'prueba';
```

### Darle permisos a ese role

```sql
GRANT SELECT ON empresa.* TO 'prueba';
```

### Ver los privilegios de un rol

```sql
SHOW GRANTS FOR 'app_developer';
```

### Asignar el role a un usuario

```sql
GRANT 'prueba' TO 'Alberto'@'%';
```

### Eliminar el role a un usuario

```sql
CREATE ROLE 'prueba';
```

### Asignar roles para esa sesión

Se utiliza para activar uno o más roles para la sesión actual

```sql
SET ROLE 'prueba';
```

### Ver roles asignados en un usuario

```sql
SHOW GRANTS FOR 'Alberto'@'%';
```
