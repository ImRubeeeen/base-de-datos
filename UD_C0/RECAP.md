# Unidad C0: Recapitulación

Autor: __Rubén__

Vamos a ver los conceptos básicos de lo que es la base de datos, como se conectan entre sí, como gestionarlas y los diferentes tipos de gestores.

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Las bases de datos son donde puedes almacenar muchos datos, relacionarlos entre sí, ordenarlos y tener un acceso más directo y rápido. Una de las cosas que resuelven es que no se necesiten usar archivos, usar directorios/archivos no es lo mejor, ya que si tuviesemos que hacerlo de esta forma y por ejemplo relaciosemos imágenes, más de 1 imágen podría estar en muchos directorios y sería un lío. [[1](#Referencias)]

## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

El DBMS es un software que se encarga de administrar las bases de datos. Puedes modificar, extraer y almacenar información de las bases de datos.

### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB: No es software libre y sigue el modelo Cliente-Servidor 
* IBM Db2: sigue el modelo Cliente-Servidor
* SQLite: Software libre y sigue el modelo Cliente-Servidor
* MariaDB: Software libre 
* SQL Server: 
* PostgreSQL: Software libre
* mySQL: Tiene doble licencia (GPL) y comercial. Sigue el modelo Cliente-Servidor
* Mongo DB: Software libre
* Microsoft Access: No es software libre y sigue el modelo Cliente-Servidor

[[2](#Referencias)]

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?

* __Cliente__: Equipo o software que se encarga de enviar peticiones al servidor y recibe respuesta.
* __Servidor__: Equipo o software que recibe las peticiones del cliente y le manda respuestas.
* __Red__: Es la estructura donde se conecta el servidor y cliente para poder comunicarse.
* __Puerto de escucha__: El puerto de escucha es el que se conecta el cliente después de haber entrado a la red. Es un número que identifica ese servicio.
* __Petición__: Es el "mensaje" que el cliente manda al servidor para después recibir una respuesta.
* __Respuesta__: Una vez que el servidor ha recibido esas peticiones, manda las respuestas al cliente.

El modelo cliente-servidor se basa en tener un servidor (puede ser un equipo o un software)que se encarga de recibir las peticiones del cliente (manda peticiones y recibe respuestas). El cliente (también puede ser un equipo o software) se conecta a los puertos del servidor (puerto de escucha ) por la red. De esta forma se puede conectar desde cualquier parte del mundo. [[3](#Referencias)]

![](cliente-servidor.jpg)

## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?

SQL es un lenguaje de consulta hecho para gestionar datos (almacenar, gestionar y recuperar datos) en una base de datos relacional. [[4](#Referencias)]

### Instrucciones de SQL

* DDL: Data Definition Lenguage (Create - Alter - Drop)
* DML: Data Manipulation Lenguage (Update - Select - Insert)
* DCL: Data Control Lenguage (Grant - Revoke)
* TCL: Transaction Control Lenguage (Commit - Rollback)

## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?

Una base de datos relacional que organiza los datos en tablas y filas. Las ventajas que tiene es que es sencillo de usar, poca redundancia de los datos y alta consistencia. La conforman los siguientes elementos:

* __Relación (tabla)__: Es un formato que usa, para explicar de forma visual como estan organizados los datos.
* __Atributo/Campo (columna)__: Cada columna tiene una caracteristica de los datos que se almacenan en la tabla.
* __Registro/Tupla (fila)__: Cada uno de ellos, tiene un valor para cada columna que define sus propiedades.

[[6](#Referencias)]

## Referencias
[1] Explicación de clase.

[2] (https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/oracle-database/ + Wikipedia y para algunos que no estaban tan claras, el Copilot).

[3] Todas las explicaciones están basadas en lo que entendí en clase con la imágen.

[4] https://aws.amazon.com/es/what-is/sql/

[5] Explicación de clase.

[6] https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/bases-de-datos-relacionales/