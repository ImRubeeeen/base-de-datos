#Query-9/Reto2.3/Canciones de la playlist con más canciones

USE Chinook;
SELECT p.PlaylistId, COUNT(*)
FROM track as t
JOIN Playlisttrack AS p
ON t.TrackId = p.TrackId
GROUP BY t.TrackId;