#Query-14/Reto2.3/Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.
SELECT *
FROM Album;

#mi intento de consulta
SELECT 
	A.Name
	(SELECT MIN(AlbumId) FROM Album WHERE ArtistId = A.ArtistId GROUP BY ArtistId) AS Ultimo_Album
FROM Artist AS A;

#consulta bien hecha
SELECT A.Name AS Nombre_Artista, AL.Title AS Ultimo_Album
FROM Artist A
INNER JOIN (
    SELECT ArtistId, MAX(AlbumId) AS Ultimo_AlbumId
    FROM Album
    GROUP BY ArtistId
) AS UltimosAlbums ON A.ArtistId = UltimosAlbums.ArtistId
INNER JOIN Album AS AL ON UltimosAlbums.Ultimo_AlbumId = AL.AlbumId;