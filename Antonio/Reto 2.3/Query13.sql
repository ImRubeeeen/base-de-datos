#Query-13/Reto2.3/Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

#Con Join
SELECT E.EmployeeId, E.FirstName, E.LastName, COUNT(C.customerId) AS NumClientes
FROM Employee AS E
JOIN Customer AS C ON E.EmployeeId = C.SupportRepId
GROUP BY E.EmployeeId, E.FirstName, E.LastName;

#Con subconsulta
SELECT 
    E.EmployeeId, 
    E.FirstName, 
    E.LastName, 
    (SELECT COUNT(*) FROM Customer WHERE SupportRepId = E.EmployeeId) AS NumClientes
FROM 
    Employee AS E;