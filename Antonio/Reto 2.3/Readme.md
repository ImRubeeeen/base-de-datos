# Reto: Subconsultas 2.3

Antonio Esteban Lorenzo.
1 DAW.Bases de datos

En este reto trabajamos con la base de datos  `chinook_MySql`, que nos viene dado en el fichero `Chinook_MySql.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos/-/tree/main/Reto%202.3?ref_type=heads


## Query 1
**Query-1/Reto2.3/Obtener las canciones con una duración superior a la media**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT *
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM Track);
```
En la subconsulta calculo la duración media de todas las canciones en milisegundos.
Selecciono todas las columnas de la tabla Track para aquellas filas donde la duración en milisegundos (Milliseconds) es mayor que la duración media calculada en la subconsulta.

## Query 2
**Query-2/Reto2.3/Listar las facturas del cliente cuyo email es “emma_jones@hotmail.com”.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
#consulta para averiguar el cliente
USE Chinook;
SELECT *
FROM Customer
WHERE Email = "emma_jones@hotmail.com";

#Consuta definitiva
USE Chinook;
SELECT *
FROM Invoice
WHERE CustomerId = (SELECT CustomerId 
					FROM Customer
					WHERE Email = "emma_jones@hotmail.com");
```

Primero hago una consulta para poder sacar el cliente con ese email, y luego la incorporo como subconsulta.
En la consulta definitiva, selecciono todas las columnas de la tabla Invoice para aquellas filas donde el CustomerId coincide con el CustomerId obtenido en la subconsulta. Así busco todas las facturas asociadas al cliente que tiene el correo electrónico "emma_jones@hotmail.com".

## Query 3
**Query-3/Reto2.3/Mostrar las listas de reproducción en las que hay canciones de reggae.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
#Consulta para buscar el género
USE Chinook;
SELECT *
FROM Genre
WHERE Name = "reggae";

#Consulta para buscar las canciones con este género
USE Chinook;
SELECT TrackId
FROM Track
WHERE GenreId in (SELECT GenreId
FROM Genre
WHERE Name = "reggae");

#Consulta definitiva
USE Chinook;
SELECT PlaylistId, Name
FROM Playlist
WHERE PlaylistId IN ( SELECT pt.PlaylistId
    FROM PlaylistTrack pt
    JOIN Track t ON pt.TrackId = t.TrackId
    JOIN Genre g ON t.GenreId = g.GenreId
    WHERE g.Name = 'Reggae');
```
Primero Selecciono las listas de reproducción que contienen canciones de género "Reggae"
`pt.PlaylistTrack` Esto hace referencia a la tabla que relaciona las listas de reproducción con las canciones que contienen.
`Track t ON pt.TrackId = t.TrackId` Une la tabla de pistas con la tabla de relación de listas de reproducción a través del identificador de pista.
`Genre g ON t.GenreId = g.GenreId` Une la tabla de géneros con la tabla de pistas para encontrar las canciones que son del género "Reggae".
`WHERE g.Name = 'Reggae'` Filtra las filas para encontrar las canciones que son del género "Reggae".
Utilizo la cláusula `WHERE ... IN` para seleccionar las listas de reproducción cuyos identificadores (PlaylistId) se encuentran en los resultados de la subconsulta.

## Query 4
**#Query-4/Reto2.3/Obtener la información de los clientes que han realizado compras superiores a 20€.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
#Consulta de clientes con compras superiores a 20
USE Chinook;
SELECT CustomerId
from Invoice
WHERE Total > 20;

#Consulta definitiva
USE Chinook;
SELECT *
FROM Customer
WHERE CustomerId in (SELECT CustomerId
from Invoice
WHERE Total > 20);
```

Utilizo la lista de CustomerId obtenida en la primera consulta como criterio para seleccionar las filas de la tabla Customer donde el CustomerId está presente en esa lista.
Esto devuelve toda la información de los clientes que han realizado compras superiores a 20€.


## Query 5
**Query-5/Reto2.3/Álbumes que tienen más de 15 canciones, junto a su artista.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT * FROM Album JOIN Artist USING (ArtistId)
WHERE AlbumId IN (
	SELECT AlbumId
    FROM Track
    GROUP BY AlbumId
    HAVING COUNT(*) > 15
);
```

En la subconsulta selecciono los AlbumId de la tabla Track agrupados por AlbumId y luego los filtro por aquellos grupos que tienen más de 15 canciones.
En la consulta principal, realizo un JOIN entre las tablas Album y Artist utilizando el campo ArtistId para obtener información del artista de cada álbum.
Luego, lo filtro solo por los álbumes cuyo AlbumId está presente en la lista obtenida en la subconsulta anterior, lo que significa que estos álbumes tienen más de 15 canciones.


## Query 6
**Query-6/Reto2.3/Obtener los álbumes con un número de canciones superiores a la media**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT AlbumId, COUNT(*) AS N_Canciones
    FROM Track
    GROUP BY AlbumId
    HAVING N_Canciones > (
		SELECT AVG(N_Canciones) FROM
		(
			SELECT AlbumId, COUNT(*) AS N_Canciones
			FROM Track
			GROUP BY AlbumId
		) AS Album_Ncanciones
	);
```

En la primera parte de la consulta calculo el número de canciones por cada álbum. Se agrupan las canciones por AlbumId y se cuenta cuántas hay en cada grupo.
Luego se calcula la media del número de canciones por álbum. Se realiza una subconsulta que calcula el número de canciones por álbum y luego se calcula la media de esa cantidad.
En la parte final, se seleccionan los álbumes cuyo número de canciones es superior a la media calculada en la subconsulta anterior.

## Query 7
**#Query-7/Reto2.3/Obtener los álbumes con una duración total superior a la media.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT AlbumId, SUM(Milliseconds) AS suma
FROM Track
GROUP BY AlbumId;
```
No se hacer esta consulta.

## Query 8
**Query-8/Reto2.3/Canciones del género con más canciones.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
#Consulta sacar las canciones del género 1
USE Chinook;
SELECT TrackId, GenreId, Name
FROM Track
WHERE GenreId = 1;

#Contar las canciones del género
USE Chinook;
SELECT GenreId, COUNT(Name)
FROM Track
GROUP BY GenreId
LIMIT 1;

#Consulta definitiva
USE Chinook;
SELECT TrackId, GenreId, Name
FROM Track
WHERE GenreId = (
    SELECT GenreId
    FROM (
        SELECT GenreId, COUNT(Name)
        FROM Track
        GROUP BY GenreId
        ORDER BY COUNT(Name) DESC
        LIMIT 1
    ) AS max_genre
);
```
En esta consulta primero encuentro el género con la mayor cantidad de canciones utilizando una subconsulta. La subconsulta cuenta el número de canciones en cada género, las ordena en orden descendente según el recuento y selecciona solo el primer género utilizando LIMIT 1. Luego, la consulta externa selecciona todas las canciones que pertenecen a este género encontrado.

## Query 9
**Query-9/Reto2.3/Canciones de la playlist con más canciones**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT p.PlaylistId, COUNT(*)
FROM track as t
JOIN Playlisttrack AS p
ON t.TrackId = p.TrackId
GROUP BY t.TrackId;
```

No se hacer esta consulta

## Query 10
**Query-10/Reto2.3/Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
#Consulta, suma total compras por cliente
USE Chinook;
SELECT CustomerId, SUM(Total) as total_compras
FROM invoice
GROUP BY CustomerId;

#Bien hecha con Join
USE Chinook;
SELECT C.FirstName, C.LastName, I.CustomerId, SUM(I.Total) as total_compras
FROM Customer AS C
JOIN Invoice AS I ON C.CustomerId = I.CustomerId
GROUP BY I.CustomerId;

#Con subconsulta
USE Chinook;
SELECT C.FirstName, C.LastName, C.CustomerId,
    (SELECT SUM(I.Total) FROM Invoice AS I WHERE I.CustomerId = C.CustomerId) AS total_compras
FROM Customer AS C;
```

Primero hago una consulta para averiguar la suma total por cliente y así luego utilizarla en la subconsulta.
La he realizado con JOIN y con Subconsulta.
La subconsulta es de SELECT, donde colocamos la primera consulta que hicimos al principio.

## Query 11
**Query-11/Reto2.3/Obtener empleados y el número de clientes a los que sirve cada uno de ellos.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT SupportRepId, COUNT(DISTINCT CustomerId)
FROM Customer
GROUP BY SupportRepId;

#Con subconsultas
USE Chinook;
SELECT E.EmployeeId, E.FirstName, E.LastName,
(SELECT COUNT(DISTINCT C.CustomerId) FROM Customer AS C WHERE C.SupportRepId = E.EmployeeId GROUP BY C.SupportRepId) AS Total_clientes
FROM Employee AS E;

#Con join
USE Chinook;
SELECT E.EmployeeId, E.FirstName, E.LastName, COUNT(DISTINCT C.CustomerId) AS Total_clientes
FROM Employee AS E
JOIN  Customer AS C ON C.SupportRepId = E.EmployeeId
GROUP BY C.SupportRepId;
```

Esta consulta la hice con JOIN y con Subconsulta, la diferencia es que con los join no te salen los null y en la subconsulta si, primero hice una consulta para contar los clientes por el supportId para luego utilizarlo en la subconsulta.

## Query 12
**Query-12/Reto2.3/Ventas totales de cada empleado.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
#Saco cada empleado a quien le a vendido
USE Chinook;
SELECT E.EmployeeId, C.CustomerId
FROM Employee AS E
JOIN Customer AS C ON E.EmployeeId = C.SupportRepId
WHERE E.EmployeeId = 3;

#Obtener las facturas de los clientes
USE Chinook;
SELECT I.InvoiceId, I.CustomerId, SUM(Total)
FROM Invoice AS I
GROUP BY I.InvoiceId
ORDER BY I.CustomerId;
```


## Query 13
**Query-13/Reto2.3/Obtener empleados y el número de clientes a los que sirve cada uno de ellos.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT E.EmployeeId, E.FirstName, E.LastName, COUNT(C.customerId) AS NumClientes
FROM Employee AS E
JOIN Customer AS C ON E.EmployeeId = C.SupportRepId
GROUP BY E.EmployeeId, E.FirstName, E.LastName;
```
Esta query no la tengo bien resuelta, no sabia como hacerla.

## Query 14
**Query-14/Reto2.3/Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
#mi intento de consulta
USE Chinook;
SELECT 
	A.Name
	(SELECT MIN(AlbumId) FROM Album WHERE ArtistId = A.ArtistId GROUP BY ArtistId) AS Ultimo_Album
FROM Artist AS A;

#consulta bien hecha
USE Chinook;
SELECT A.Name AS Nombre_Artista, AL.Title AS Ultimo_Album
FROM Artist A
INNER JOIN (
    SELECT ArtistId, MAX(AlbumId) AS Ultimo_AlbumId
    FROM Album
    GROUP BY ArtistId
) AS UltimosAlbums ON A.ArtistId = UltimosAlbums.ArtistId
INNER JOIN Album AS AL ON UltimosAlbums.Ultimo_AlbumId = AL.AlbumId;
```



