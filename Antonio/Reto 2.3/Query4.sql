#Query-4/Reto2.3/Obtener la información de los clientes que han realizado compras superiores a 20€.

#Consulta de clientes con compras superiores a 20
USE Chinook;
SELECT CustomerId
from Invoice
WHERE Total > 20;

#Consulta definitiva
USE Chinook;
SELECT *
FROM Customer
WHERE CustomerId in (SELECT CustomerId
from Invoice
WHERE Total > 20);

# 27/05/24
SELECT *
FROM customer;

SELECT CustomerId, SUM(Total) AS suma
FROM invoice
GROUP BY InvoiceId
HAVING suma > 20;

SELECT *
FROM customer
WHERE CustomerId in (
	SELECT CustomerId 
	FROM invoice
	GROUP BY InvoiceId
	HAVING  SUM(Total) > 20);



