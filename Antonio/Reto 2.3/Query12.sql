#Query-12/Reto2.3/Ventas totales de cada empleado.

#Saco cada empleado a quien le a vendido
USE Chinook;
SELECT E.EmployeeId, C.CustomerId
FROM Employee AS E
JOIN Customer AS C ON E.EmployeeId = C.SupportRepId
WHERE E.EmployeeId = 3;

#Obtener las facturas de los clientes
USE Chinook;
SELECT I.InvoiceId, I.CustomerId, SUM(Total)
FROM Invoice AS I
GROUP BY I.InvoiceId
ORDER BY I.CustomerId;

# 27/05/24
USE Chinook;
SELECT *
FROM Employee;

SELECT *
FROM Invoice;

SELECT *
FROM Customer;

-- Total del cliente de las facturas del cliente 1
SELECT SUM(Total)
FROM Invoice
WHERE CustomerId = 1;

-- clientes de cada empleado
SELECT CustomerId
FROM Customer
WHERE SupportRepId = 3;

-- total de las facturas de varios clientes
SELECT SUM(Total)
FROM Invoice
WHERE CustomerId IN (1, 3, 12);

-- TOtal facturas de un empleado
SELECT SUM(Total)
FROM Invoice
WHERE CustomerId IN (
	SELECT CustomerId
	FROM Customer
	WHERE SupportRepId = 3);
    
-- Datos del empleado
SELECT EmployeeId, LastName, FirstName, (
SELECT SUM(Total)
FROM Invoice
WHERE CustomerId IN (
	SELECT CustomerId
	FROM Customer
	WHERE SupportRepId = E.EmployeeId
    )
)AS Total_ventas
FROM Employee AS E;