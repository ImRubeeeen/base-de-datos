#Query-2/Reto2.3/Listar las facturas del cliente cuyo email es “emma_jones@hotmail.com”.

#Subconsulta
USE chinook;
SELECT *
FROM Customer
WHERE Email = "emma_jones@hotmail.com";

#Consuta definitiva
USE chinook;
SELECT *
FROM Invoice
WHERE CustomerId = (SELECT CustomerId 
					FROM Customer
					WHERE Email = "emma_jones@hotmail.com");

#27/05/24
SELECT *
FROM invoice
WHERE CustomerId = (SELECT CustomerId FROM Customer WHERE Email Like 'emma_jones@hotmail.com');