#Query-3/Reto2.3/Mostrar las listas de reproducción en las que hay canciones de reggae.
USE Chinook;
SELECT *
FROM Genre
WHERE Name = "reggae";

USE Chinook;
SELECT TrackId
FROM Track
WHERE GenreId in (SELECT GenreId
FROM Genre
WHERE Name = "reggae");


USE Chinook;
SELECT PlaylistId, Name
FROM Playlist
WHERE PlaylistId IN ( SELECT pt.PlaylistId
    FROM PlaylistTrack pt
    JOIN Track t ON pt.TrackId = t.TrackId
    JOIN Genre g ON t.GenreId = g.GenreId
    WHERE g.Name = 'Reggae');


# 27/05/24
SELECT *
FROM Track
WHERE GenreId = (
	SELECT GenreId
	FROM genre
	WHERE Name LIKE 'reggae');