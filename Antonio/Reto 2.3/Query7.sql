#Query-7/Reto2.3/Obtener los álbumes con una duración total superior a la media.

USE Chinook;
SELECT AlbumId, SUM(Milliseconds) AS suma
FROM Track
GROUP BY AlbumId;

# 27/05/24

SELECT *
FROM Track;

-- La SUMA de duracion de cada album
SELECT AlbumId, SUM(Milliseconds)
FROM Track
GROUP BY AlbumId;

-- La media de la suma de las canciones por album
SELECT AVG(suma) 
FROM(
	SELECT SUM(Milliseconds) AS suma
	FROM Track
	GROUP BY AlbumId) AS AA;
    
    
SELECT Title
FROM Album;

SELECT AVG(suma) 
FROM(
	SELECT SUM(Milliseconds) AS suma
	FROM Track
	GROUP BY AlbumId) AS AA;

# clase 27/05/24
-- Duracion total de un album
SELECT *
FROM Album;

-- Duracion de cada pista del album 1
SELECT Milliseconds
FROM Track
WHERE AlbumId = 1;

-- Duracion total del album
SELECT SUM(Milliseconds)
FROM Track
WHERE AlbumId = 1;

-- Duracion total de cada album
SELECT AlbumId, SUM(Milliseconds) AS duracionTotal
FROM Track
GROUP BY AlbumId;

-- Albumes con duracion total superior a un numero
SELECT AlbumId, SUM(Milliseconds) AS duracionTotal
FROM Track
GROUP BY AlbumId
HAVING duracionTotal > (2500000);


-- OTRA OPCION
SELECT AlbumId, duracionTotal
FROM (
SELECT AlbumId, SUM(Milliseconds) AS duracionTotal
FROM Track
GROUP BY AlbumId
) AS AlbumDuracion
WHERE duracionTotal > (2500000);



-- Media de la duracion de los albumes
SELECT AVG(duracionTotal)
FROM(
SELECT AlbumId, SUM(Milliseconds) AS duracionTotal
FROM Track
GROUP BY AlbumId
) AS tablaDuracion;

-- Albumes cuya duyracion superior a la media
SELECT AlbumId, SUM(Milliseconds) AS duracionTotal
FROM Track
GROUP BY AlbumId
HAVING duracionTotal > (SELECT AVG(duracionTotal)
FROM(
SELECT AlbumId, SUM(Milliseconds) AS duracionTotal
FROM Track
GROUP BY AlbumId
) AS tablaDuracion
);


SELECT AlbumId, Title, SUM(Milliseconds) AS duracionTotal
FROM Track
JOIN Album
using (AlbumId)
GROUP BY AlbumId
HAVING duracionTotal > (SELECT AVG(duracionTotal)
FROM(
SELECT AlbumId, SUM(Milliseconds) AS duracionTotal
FROM Track
GROUP BY AlbumId
) AS tablaDuracion
);


