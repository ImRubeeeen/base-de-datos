#Query-8/Reto2.3/Canciones del género con más canciones.

#Consulta sacar las canciones del género 1
USE Chinook;
SELECT TrackId, GenreId, Name
FROM Track
WHERE GenreId = 1;

#Contar las canciones del género
USE Chinook;
SELECT GenreId, COUNT(Name)
FROM Track
GROUP BY GenreId
LIMIT 1;

#Consulta definitiva
USE Chinook;
SELECT TrackId, GenreId, Name
FROM Track
WHERE GenreId = (
    SELECT GenreId
    FROM (
        SELECT GenreId, COUNT(Name)
        FROM Track
        GROUP BY GenreId
        ORDER BY COUNT(Name) DESC
        LIMIT 1
    ) AS max_genre
);