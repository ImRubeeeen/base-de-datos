#Muestra nombre y apellidos de los empleados que tienen clientes asignados. (Pista: se puede hacer con EXISTS).
USE Chinook;
SELECT FirstName, LastName
FROM Employee
WHERE EmployeeId in (
	SELECT DISTINCT SupportRepId 
    FROM Customer
    WHERE SupportRepId IS NOT NULL
);

-- voy a resolverlo con exist
SELECT FirstName, LastName
FROM Employee
	WHERE EXISTS 
    (
    SELECT *
    FROM Customer
    WHERE SupportRepId = EmployeeId
    );
    