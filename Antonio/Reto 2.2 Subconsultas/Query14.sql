#Clasifica a los clientes en tres categorías según su país: “Local” si el país es
#‘USA’, “Internacional” si el país es distinto de ‘USA’, y “Desconocido” si el país es nulo.ok
SELECT CustomerId, FirstName, LastName, Country,
    CASE
        WHEN Country = 'USA' THEN 'Local'
        WHEN Country IS NULL THEN 'Desconocido'
        ELSE 'Internacional'
    END AS Categoria
FROM Customer;