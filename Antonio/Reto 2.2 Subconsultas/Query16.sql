#Clasifica a los empleados en diferentes niveles según su cargo (manager, asistente o empleado).ok
USE chinook;
SELECT EmployeeId, FirstName, LastName, Title,
	CASE
		WHEN Title = "General Manager" OR Title = "Sales Manager" THEN "Manager"
        WHEN Title = "IT Staff" THEN "Asistente"
        ELSE "empleado"
	END AS "Categoria"
FROM Employee;

