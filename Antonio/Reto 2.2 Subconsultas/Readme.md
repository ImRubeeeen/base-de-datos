# Reto: Subconsultas

Antonio Esteban Lorenzo.
1 DAW.Bases de datos

En este reto trabajamos con la base de datos  `chinook_MySql`, que nos viene dado en el fichero `Chinook_MySql.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Tonet44/bases-de-datos/-/tree/main/Subconsultas?ref_type=heads

## Query 1
**Muestra las listas de reproducción cuyo nombre comienza por M, junto a las 3 primeras canciones de cada uno, ordenadas por álbum y por precio. (más bajo primero)**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT *
FROM Playlist AS P
JOIN PlaylistTrack AS P1
ON P.PlaylistId = P1.PlaylistId
JOIN Track AS T
ON P1.TrackId = T.TrackId
JOIN Album AS A
ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE 'M%'
ORDER BY A.Title, T.UnitPrice DESC;
```
Esta Query no la tengo bien resuelta ya que me faltaría limitar a 3 canciones con alguna subconsulta.
He conseguido que me salgan las canciones de las listas de reproducción que empiezen por la letra "M" y ordenarlas por album y precio.

## Query 2
**Muestra todos los artistas que tienen canciones con duración superior a 5 minutos**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT A.Name, T.Name AS "titulo cancion", T.Milliseconds
FROM Artist AS A
JOIN Album AS A2
ON A.ArtistId = A2.ArtistId
JOIN Track AS T
ON A2.AlbumId = T.AlbumId
WHERE T.Milliseconds > 300000
ORDER BY T.Milliseconds DESC;
```
En el `SELECT` he puesto solo tres columnas de información para poder ver más claro la solución, ademas a `T.Name` le cambie el nombre por "titulo canción" para que se viera más claro.
Para poder realizar esta consulta necestiba relacionar tres tablas y relacionarlas entre ellas con `JOIN` a traves de sus claves primarias y foraneas.
Luego en el `WHERE` he filtrado con la columna de `Milliseconds` para que me slgan solo las que tengan más de 300000 segundos, pongo esta cifra ya que en el enunciado te pide 5 minutos y en la tabla está en milisegundos.
Por último lo he ordenado de forma descendente por la tabla de Milisegundos.

## Query 3
**Muestra nombre y apellidos de los empleados que tienen clientes asignados. (Pista: se puede hacer con EXISTS).**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT FirstName, LastName
FROM Employee
	WHERE EXISTS 
    (
    SELECT *
    FROM Customer
    WHERE SupportRepId = EmployeeId
    );

    -- OTRA OPCIÓN
USE Chinook;
SELECT FirstName, LastName
FROM Employee
WHERE EmployeeId in (
	SELECT DISTINCT SupportRepId 
    FROM Customer
    WHERE SupportRepId IS NOT NULL
);
```
Seleccionamos el nombre y el apellido de la tabla `Employee`, en WHERE vamos a utilazr el `EXIST`.
El `EXISIT` preguntaría si existe en la tabla `Customer` algún ` SupportRepId = EmployeeId` si hay alguno que coincida nos saldrá en la tabla el nombre y el apellido.

## Query 4
**Muestra todas las canciones que no han sido compradas**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT T.TrackId, T.Name 
FROM Track AS T
WHERE T.TrackId NOT IN (
	SELECT InvoiceLine.TrackId FROM InvoiceLine
);
```

En el `SELECT` selecciono las columnas de `TrackId` y `Name` porque es la información que deseo que salga de la tabla `Track`
Luego realizo una consulta de WHERE utilizando el `NOT IN` de conexión para saber los que no están en la tabla de `InvoiceLine`en la columna de TrackId.

## Query 5
**Lista los empleados junto a sus subordinados (empleados que reportan a ellos)**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT *
FROM Customer AS C
JOIN Employee
ON Customer.SupportRepId = Employee.EmployeeId;
```
No sé como hacerla.

## Query 6
**Muestra todas las canciones que ha comprado el cliente Luis Rojas**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
-- MI SOLUCIÓN
USE Chinook;
SELECT Customer.CustomerId, Invoice.InvoiceDate, InvoiceLine.Quantity, Track.Name
FROM Customer
JOIN Invoice
ON Customer.CustomerId = Invoice.CustomerId
JOIN InvoiceLine
ON Invoice.InvoiceId = InvoiceLine.InvoiceLineId
JOIN Track
ON InvoiceLine.TrackId = Track.TrackId
WHERE LastName = "Rojas" AND FirstName = "Luis";

-- SOLUCIÓN CLASE
SELECT *
FROM Track
WHERE TrackId in
(
	SELECT TrackId FROM InvoiceLine
    WHERE InvoiceLineId IN (
		SELECT InvoiceId FROM Invoice
        WHERE CustomerId IN (
			SELECT CustomerId FROM Customer
            WHERE FirstName = "Luis" AND LastName = "Rojas"
            )
		)
	);
```
Para poder realizar está consulta hay que relacionar cuatro tablas `Customer`, `Invoice`, `InvoiceLine` y `Track`.
y se relacionan entre ellas de la siguiente forma:
`ON Customer.CustomerId (PK)= Invoice.CustomerId(FK)`
`ON Invoice.InvoiceId(PK) = InvoiceLine.InvoiceLineId(FK)`
`ON InvoiceLine.TrackId(FK) = Track.TrackId(PK)`
Por ultimo en el `WHERE` filtrO los resultados para incluir solo las compras realizadas por el cliente con el apellido "Rojas" y el nombre "Luis". Esto busca los resultados solo con las compras asociadas con este cliente específico.


## Query 7
**Canciones que son más caras que cualquier otra canción del mismo álbum**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql

```
En esta Query no encuentro el sentido del enunciado, no sé exactamente que es lo que pides.

## Query 8
**Clientes que han comprado todos los álbumes de un artista específico**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
-- MI SOLUCIÓN (NO FUNCIONA CORRECTAMENTE)
USE Chinook;
SELECT c.CustomerId, c.FirstName, c.LastName
FROM Customer c
JOIN Invoice i ON c.CustomerId = i.CustomerId
JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId
JOIN Track t ON il.TrackId = t.TrackId
JOIN Album a ON t.AlbumId = a.AlbumId
WHERE a.ArtistId = 51
GROUP BY c.CustomerId, c.FirstName, c.LastName
HAVING COUNT(DISTINCT a.AlbumId) = (
    SELECT COUNT(*) 
    FROM Album 
    WHERE ArtistId = 51
);

-- SOLUCIÓN CLASE
USE Chinook;
SELECT *
FROM Album
WHERE ArtistId = (
select ArtistId from Artist
where Name = "Queen"
);
```
Voy a explicar la solución de clase ya que la mia no está bien. La probé con el usuario 1 y creía que estaba bien, pero al probarlo con el 51 que es el que hicmos en clase no me da el mismo resultado.

En el `SELECT` seleccionamos todas las columnas de la tabla `Album` y en el `WHERE` atraves del `ArtistId` que es la clave foranea que conecta con la tabla `Album` vamos a realizar una subconsulta para buscar un artista concreto, en este caso Queen.

## Query 9
**Clientes que han comprado más de 20 canciones en una sola transacción**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE chinook;
SELECT *
FROM INVOICE, invoiceline;
```
No sé como hacerla.

## Query 10
**Muestra las 10 canciones más compradas**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT Track.Name AS Nombre_canción, COUNT(*) AS Compras
FROM Track
JOIN InvoiceLine
ON Track.TrackId = InvoiceLine.TrackId
GROUP BY Track.TrackId, Track.Name
ORDER BY Compras DESC
LIMIT 10;
```
`SELECT Track.Name AS Nombre_canción, COUNT(*) AS Compras ` En esta línea, he seleccionando dos columnas. La primera columna es el nombre de la canción, que se obtiene de la tabla `Track`. La segunda columna es el recuento de las compras de cada canción, que se obtiene utilizando la función de agregación `COUNT(*)`.
Para esta consulta he necesitado dos tablas `Track` y `InvoiceLine` y las he tenido que relacionar entre si de la siguiente forma, `ON Track.TrackId(PK) = InvoiceLine.TrackId(FK)`
Luego lo he agrupado por el identificador de la canción y por el nombre de la canción y los he ordenado por compras poniendo un limite de 10 de filas. 

## Query 11
**Muestra las canciones con una duración superior a la media**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT Name, Milliseconds
FROM Track
WHERE Milliseconds > (SELECT  AVG(Milliseconds) FROM Track)
ORDER BY Milliseconds;
```
En esta consulta he tenido que realizar una subconsulta para poder sacar la media de la columna de los `Miliseconds` de la tabla `Track` para luego compararla con el `WHERE` para que me salgan solo los que sean superiores a la media.


## Query 12
**Para demostrar lo bueno que es nuestro servicio, muestra el número de países donde tenemos clientes, el número de géneros músicales de los que disponemos y el número de pistas.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT  COUNT(DISTINCT Country) AS "Número de paises",
	(SELECT COUNT(DISTINCT Name) FROM  Genre) AS "Número de generos",
    (SELECT COUNT(DISTINCT Name) FROM Track) AS "Número de pistas"
FROM Customer;
```
Primero he utilizado una función de agregación COUNT para contar el número de paises que hay en la columna `Country` en la tabla `Customer`.
Para poder sacar los otros dos datos hice dos subconsultas:
La primer para sacar el número de géneros, utilice la función COUNT de la Tabla `Genre` y de la columna `Name`
La segunda para sacar el numero de pistas, utilice la función COUNT de la Tabla `Track` y de la columna `Name`

## Query 13
**Canciones vendidas más que la media de ventas por canción.**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT Name,
	(SELECT AVG(Quantity) FROM invoiceline) AS "media",
    (SELECT AVG(invoiceId) FROM invoiceline) AS "canciones_vendidas"
FROM track
WHERE "media" > "canciones_vendidas";
```
Esta query no la tengo bien resuelta, no sabia como hacerla.

## Query 14
**Clasifica a los clientes en tres categorías según su país: “Local” si el país es ‘USA’, “Internacional” si el país es distinto de ‘USA’, y “Desconocido” si el país es nulo**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT CustomerId, FirstName, LastName, Country,
    CASE
        WHEN Country = 'USA' THEN 'Local'
        WHEN Country IS NULL THEN 'Desconocido'
        ELSE 'Internacional'
    END AS Categoria
FROM Customer;
```

En el `SELECT` selecciono cuatro columnas de la tabla `Customer`, `CustomerId`, `FirstName`, `LastName` y `Country`.
He utilizado una expresión condicional `CASE` y dentro de ella puse unos condicionantes.
`WHEN Country = 'USA' THEN 'Local'` Si el valor en la columna Country es 'USA', el cliente se clasifica como "Local".
`WHEN Country IS NULL THEN 'Desconocido'` Si el valor en la columna Country es nulo, el cliente se clasifica como "Desconocido".
`ELSE 'Internacional'` Si ninguna de las condiciones anteriores se cumple, se asume que el cliente es "Internacional".
`END AS Categoria` Con esto marco el final de la expresión `CASE` y le doy un nombre "Categoria".


## Query 15
**Calcula el descuento aplicado sobre cada factura, que depende del monto total de la factura (10% para facturas superiores a $100).**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT InvoiceId, Total,
	CASE 
		WHEN Total > 100 THEN Total * 0.1
        ELSE 0
	END AS Descuento_Aplicado
FROM Invoice;
```
En el `SELECT` he Seleccionado dos columnas de la tabla `Invoice`, `InvoiceId` y `Total`.
He utilizado una expresión condicional `CASE`, en ella he metido una serie de condiciones.
`WHEN Total > 100 THEN Total * 0.1`  Esto significa que si el monto total de la factura es mayor que 100, se calculará un descuento del 10%.
`ELSE 0` Si la condición anterior no se cumple, no se aplica ningún descuento, y el valor del descuento aplicado se establece en 0.
`END AS Descuento_Aplicado` con esto finalizo el CASE y le doy un nombre "Descuento_Aplicado" al resultado de la expresión condicional.


## Query 16
**Clasifica a los empleados en diferentes niveles según su cargo (manager, asistente o empleado).**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT EmployeeId, FirstName, LastName, Title,
	CASE
		WHEN Title = "General Manager" OR Title = "Sales Manager" THEN "Manager"
        WHEN Title = "IT Staff" THEN "Asistente"
        ELSE "empleado"
	END AS "Categoria"
FROM Employee;
```
En el `SELECT` he Seleccionado cuatro columnas de la tabla `Employee`, `EmployeeId`, `FirstName`, `LastName`, `Title`.
Luego he utilizado el `CASE`de forma condicional, si el título es "General Manager" o "Sales Manager", se asignará la categoría "Manager". Si el título es "IT Staff", se asignará la categoría "Asistente". Para cualquier otro título, se asignará la categoría "empleado". El resultado de esta evaluación se guarda en una columna llamada "Categoria".
En el `FROM`seleccionaremos la tabla `Employee`.


## Query 17
**Etiqueta a los clientes como “VIP” si han gastado más de $500 en compras totales**

Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE Chinook;
SELECT CustomerId, SUM(Total) AS TOTAL,
	CASE
			WHEN SUM(Total) > 500 THEN "VIP"
            ELSE "NORMAL"
	END AS "Categoria"
FROM Invoice
GROUP BY CustomerId;
```

En el `SELECT` he seleccionado la columna `CustomeId` para identificar a cada cliente y la función SUM(Total) la utilizo para calcular el total gastado por cada cliente en todas sus facturas. El resultado de esta suma se muestra como TOTAL en el resultado de la consulta.
Luego he puesto un `CASE` para evaluar la suma total gastada por cada cliente. Si la suma total es mayor que 500, se asigna la categoría "VIP" al cliente. De lo contrario, se asigna la categoría "NORMAL". El resultado de esta evaluación se guarda en una columna llamada "Categoria".
Con el `FROM` selecciono de la tabla llamada "Invoice".
Con el `GROUP BY` agrupo los resultados por el identificador único de cada cliente (CustomerId). Esto significa que la función de agregación SUM(Total) se calculará para cada cliente individualmente, y no para todos los datos en la tabla.

