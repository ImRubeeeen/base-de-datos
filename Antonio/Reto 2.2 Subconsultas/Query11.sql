#Muestra las canciones con una duración superior a la media.ok
USE chinook;
SELECT Name, Milliseconds
FROM Track
WHERE Milliseconds > (SELECT  AVG(Milliseconds) FROM Track)
ORDER BY Milliseconds;