#Muestra todos los artistas que tienen canciones con duración superior a 5 minutos.OK
USE Chinook;
SELECT A.Name, T.Name AS "titulo cancion", T.Milliseconds
FROM Artist AS A
JOIN Album AS A2
ON A.ArtistId = A2.ArtistId
JOIN Track AS T
ON A2.AlbumId = T.AlbumId
WHERE T.Milliseconds > 300000
ORDER BY T.Milliseconds DESC;