#Clientes que han comprado todos los álbumes de un artista específico.
SELECT c.CustomerId, c.FirstName, c.LastName
FROM Customer c
JOIN Invoice i ON c.CustomerId = i.CustomerId
JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId
JOIN Track t ON il.TrackId = t.TrackId
JOIN Album a ON t.AlbumId = a.AlbumId
WHERE a.ArtistId = 51
GROUP BY c.CustomerId, c.FirstName, c.LastName
HAVING COUNT(DISTINCT a.AlbumId) = (
    SELECT COUNT(*) 
    FROM Album 
    WHERE ArtistId = 51
);

SELECT *
FROM Album
WHERE ArtistId=51;

select * from Artist -- me quedo con artistid
where Name = "Queen";

-- todos los albumes de queen
SELECT *
FROM Album
WHERE ArtistId = (
select ArtistId from Artist
where Name = "Queen"
); 