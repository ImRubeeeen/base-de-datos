#Calcula el descuento aplicado sobre cada factura, que depende del monto total
#de la factura (10% para facturas superiores a $100).ok
USE chinook;
SELECT InvoiceId, Total,
	CASE 
		WHEN Total > 100 THEN Total * 0.1
        ELSE 0
	END AS Descuento_Aplicado
FROM Invoice;