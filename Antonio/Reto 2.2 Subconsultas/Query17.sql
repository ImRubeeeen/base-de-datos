#Etiqueta a los clientes como “VIP” si han gastado más de $500 en compras totales.ok
USE chinook;
SELECT CustomerId, SUM(Total) AS TOTAL,
	CASE
			WHEN SUM(Total) > 500 THEN "VIP"
            ELSE "NORMAL"
	END AS "Categoria"
FROM Invoice
GROUP BY CustomerId;
	