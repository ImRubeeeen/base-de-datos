#Muestra todas las canciones que ha comprado el cliente Luis Rojas.ok
USE Chinook;
SELECT Customer.CustomerId, Invoice.InvoiceDate, InvoiceLine.Quantity, Track.Name
FROM Customer
JOIN Invoice
ON Customer.CustomerId = Invoice.CustomerId
JOIN InvoiceLine
ON Invoice.InvoiceId = InvoiceLine.InvoiceLineId
JOIN Track
ON InvoiceLine.TrackId = Track.TrackId
WHERE LastName = "Rojas" AND FirstName = "Luis";


SELECT *
FROM Track
WHERE TrackId in
(
	SELECT TrackId FROM InvoiceLine
    WHERE InvoiceLineId IN (
		SELECT InvoiceId FROM Invoice
        WHERE CustomerId IN (
			SELECT CustomerId FROM Customer
            WHERE FirstName = "Luis" AND LastName = "Rojas"
            )
		)
	);