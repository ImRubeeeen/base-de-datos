#Canciones que son más caras que cualquier otra canción del mismo álbum.
SELECT *
FROM Track AS T1
WHERE UnitPrice > (
    SELECT MAX(T2.UnitPrice)
    FROM Track AS T2
    WHERE T1.AlbumId = T2.AlbumId
);